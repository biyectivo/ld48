{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Poetsen One",
  "styleName": "Regular",
  "size": 40.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 1,
  "glyphs": {
    "32": {"x":2,"y":2,"w":11,"h":65,"character":32,"shift":11,"offset":0,},
    "33": {"x":448,"y":203,"w":14,"h":65,"character":33,"shift":16,"offset":1,},
    "34": {"x":464,"y":203,"w":19,"h":65,"character":34,"shift":22,"offset":3,},
    "35": {"x":485,"y":203,"w":35,"h":65,"character":35,"shift":37,"offset":1,},
    "36": {"x":522,"y":203,"w":26,"h":65,"character":36,"shift":28,"offset":1,},
    "37": {"x":550,"y":203,"w":38,"h":65,"character":37,"shift":41,"offset":2,},
    "38": {"x":590,"y":203,"w":35,"h":65,"character":38,"shift":36,"offset":1,},
    "39": {"x":627,"y":203,"w":8,"h":65,"character":39,"shift":12,"offset":3,},
    "40": {"x":637,"y":203,"w":22,"h":65,"character":40,"shift":22,"offset":1,},
    "41": {"x":661,"y":203,"w":23,"h":65,"character":41,"shift":22,"offset":-2,},
    "42": {"x":717,"y":203,"w":22,"h":65,"character":42,"shift":25,"offset":2,},
    "43": {"x":949,"y":203,"w":24,"h":65,"character":43,"shift":26,"offset":1,},
    "44": {"x":741,"y":203,"w":10,"h":65,"character":44,"shift":14,"offset":1,},
    "45": {"x":753,"y":203,"w":20,"h":65,"character":45,"shift":24,"offset":2,},
    "46": {"x":775,"y":203,"w":10,"h":65,"character":46,"shift":13,"offset":1,},
    "47": {"x":787,"y":203,"w":17,"h":65,"character":47,"shift":17,"offset":0,},
    "48": {"x":806,"y":203,"w":31,"h":65,"character":48,"shift":35,"offset":2,},
    "49": {"x":839,"y":203,"w":17,"h":65,"character":49,"shift":20,"offset":1,},
    "50": {"x":858,"y":203,"w":28,"h":65,"character":50,"shift":31,"offset":1,},
    "51": {"x":888,"y":203,"w":28,"h":65,"character":51,"shift":30,"offset":0,},
    "52": {"x":918,"y":203,"w":29,"h":65,"character":52,"shift":32,"offset":1,},
    "53": {"x":419,"y":203,"w":27,"h":65,"character":53,"shift":30,"offset":1,},
    "54": {"x":686,"y":203,"w":29,"h":65,"character":54,"shift":33,"offset":2,},
    "55": {"x":392,"y":203,"w":25,"h":65,"character":55,"shift":26,"offset":2,},
    "56": {"x":2,"y":203,"w":29,"h":65,"character":56,"shift":32,"offset":1,},
    "57": {"x":771,"y":136,"w":29,"h":65,"character":57,"shift":32,"offset":2,},
    "58": {"x":802,"y":136,"w":12,"h":65,"character":58,"shift":15,"offset":1,},
    "59": {"x":816,"y":136,"w":12,"h":65,"character":59,"shift":15,"offset":1,},
    "60": {"x":830,"y":136,"w":21,"h":65,"character":60,"shift":23,"offset":1,},
    "61": {"x":853,"y":136,"w":25,"h":65,"character":61,"shift":29,"offset":2,},
    "62": {"x":880,"y":136,"w":20,"h":65,"character":62,"shift":23,"offset":2,},
    "63": {"x":902,"y":136,"w":26,"h":65,"character":63,"shift":28,"offset":2,},
    "64": {"x":930,"y":136,"w":40,"h":65,"character":64,"shift":42,"offset":1,},
    "65": {"x":972,"y":136,"w":32,"h":65,"character":65,"shift":33,"offset":0,},
    "66": {"x":33,"y":203,"w":28,"h":65,"character":66,"shift":32,"offset":2,},
    "67": {"x":313,"y":203,"w":31,"h":65,"character":67,"shift":33,"offset":2,},
    "68": {"x":63,"y":203,"w":32,"h":65,"character":68,"shift":35,"offset":2,},
    "69": {"x":97,"y":203,"w":24,"h":65,"character":69,"shift":27,"offset":2,},
    "70": {"x":123,"y":203,"w":24,"h":65,"character":70,"shift":25,"offset":2,},
    "71": {"x":149,"y":203,"w":31,"h":65,"character":71,"shift":34,"offset":2,},
    "72": {"x":182,"y":203,"w":32,"h":65,"character":72,"shift":36,"offset":2,},
    "73": {"x":216,"y":203,"w":13,"h":65,"character":73,"shift":17,"offset":2,},
    "74": {"x":231,"y":203,"w":25,"h":65,"character":74,"shift":25,"offset":-1,},
    "75": {"x":258,"y":203,"w":30,"h":65,"character":75,"shift":33,"offset":2,},
    "76": {"x":290,"y":203,"w":21,"h":65,"character":76,"shift":24,"offset":2,},
    "77": {"x":346,"y":203,"w":44,"h":65,"character":77,"shift":48,"offset":2,},
    "78": {"x":975,"y":203,"w":32,"h":65,"character":78,"shift":36,"offset":2,},
    "79": {"x":2,"y":270,"w":34,"h":65,"character":79,"shift":38,"offset":2,},
    "80": {"x":38,"y":270,"w":29,"h":65,"character":80,"shift":31,"offset":2,},
    "81": {"x":751,"y":270,"w":33,"h":65,"character":81,"shift":36,"offset":2,},
    "82": {"x":786,"y":270,"w":29,"h":65,"character":82,"shift":33,"offset":2,},
    "83": {"x":817,"y":270,"w":27,"h":65,"character":83,"shift":29,"offset":1,},
    "84": {"x":846,"y":270,"w":29,"h":65,"character":84,"shift":29,"offset":1,},
    "85": {"x":877,"y":270,"w":30,"h":65,"character":85,"shift":35,"offset":3,},
    "86": {"x":909,"y":270,"w":32,"h":65,"character":86,"shift":33,"offset":1,},
    "87": {"x":943,"y":270,"w":49,"h":65,"character":87,"shift":50,"offset":1,},
    "88": {"x":2,"y":337,"w":34,"h":65,"character":88,"shift":34,"offset":0,},
    "89": {"x":38,"y":337,"w":32,"h":65,"character":89,"shift":32,"offset":1,},
    "90": {"x":72,"y":337,"w":29,"h":65,"character":90,"shift":29,"offset":0,},
    "91": {"x":103,"y":337,"w":21,"h":65,"character":91,"shift":21,"offset":1,},
    "92": {"x":126,"y":337,"w":12,"h":65,"character":92,"shift":17,"offset":3,},
    "93": {"x":140,"y":337,"w":21,"h":65,"character":93,"shift":21,"offset":-1,},
    "94": {"x":163,"y":337,"w":22,"h":65,"character":94,"shift":24,"offset":1,},
    "95": {"x":187,"y":337,"w":23,"h":65,"character":95,"shift":28,"offset":1,},
    "96": {"x":212,"y":337,"w":11,"h":65,"character":96,"shift":11,"offset":0,},
    "97": {"x":225,"y":337,"w":29,"h":65,"character":97,"shift":30,"offset":0,},
    "98": {"x":256,"y":337,"w":28,"h":65,"character":98,"shift":32,"offset":2,},
    "99": {"x":286,"y":337,"w":26,"h":65,"character":99,"shift":27,"offset":1,},
    "100": {"x":314,"y":337,"w":30,"h":65,"character":100,"shift":32,"offset":1,},
    "101": {"x":346,"y":337,"w":27,"h":65,"character":101,"shift":28,"offset":1,},
    "102": {"x":722,"y":270,"w":27,"h":65,"character":102,"shift":24,"offset":1,},
    "103": {"x":691,"y":270,"w":29,"h":65,"character":103,"shift":32,"offset":1,},
    "104": {"x":661,"y":270,"w":28,"h":65,"character":104,"shift":32,"offset":2,},
    "105": {"x":333,"y":270,"w":12,"h":65,"character":105,"shift":15,"offset":2,},
    "106": {"x":69,"y":270,"w":22,"h":65,"character":106,"shift":15,"offset":-8,},
    "107": {"x":93,"y":270,"w":26,"h":65,"character":107,"shift":29,"offset":2,},
    "108": {"x":121,"y":270,"w":13,"h":65,"character":108,"shift":16,"offset":2,},
    "109": {"x":136,"y":270,"w":44,"h":65,"character":109,"shift":48,"offset":2,},
    "110": {"x":182,"y":270,"w":29,"h":65,"character":110,"shift":33,"offset":2,},
    "111": {"x":213,"y":270,"w":29,"h":65,"character":111,"shift":32,"offset":1,},
    "112": {"x":244,"y":270,"w":30,"h":65,"character":112,"shift":32,"offset":1,},
    "113": {"x":276,"y":270,"w":29,"h":65,"character":113,"shift":32,"offset":1,},
    "114": {"x":307,"y":270,"w":24,"h":65,"character":114,"shift":25,"offset":2,},
    "115": {"x":347,"y":270,"w":24,"h":65,"character":115,"shift":25,"offset":0,},
    "116": {"x":639,"y":270,"w":20,"h":65,"character":116,"shift":21,"offset":1,},
    "117": {"x":373,"y":270,"w":28,"h":65,"character":117,"shift":32,"offset":2,},
    "118": {"x":403,"y":270,"w":30,"h":65,"character":118,"shift":30,"offset":0,},
    "119": {"x":435,"y":270,"w":44,"h":65,"character":119,"shift":44,"offset":0,},
    "120": {"x":481,"y":270,"w":28,"h":65,"character":120,"shift":29,"offset":0,},
    "121": {"x":511,"y":270,"w":29,"h":65,"character":121,"shift":32,"offset":1,},
    "122": {"x":542,"y":270,"w":27,"h":65,"character":122,"shift":27,"offset":0,},
    "123": {"x":571,"y":270,"w":25,"h":65,"character":123,"shift":24,"offset":0,},
    "124": {"x":598,"y":270,"w":12,"h":65,"character":124,"shift":15,"offset":1,},
    "125": {"x":612,"y":270,"w":25,"h":65,"character":125,"shift":24,"offset":-1,},
    "126": {"x":741,"y":136,"w":28,"h":65,"character":126,"shift":32,"offset":2,},
    "160": {"x":739,"y":136,"w":0,"h":65,"character":160,"shift":11,"offset":0,},
    "161": {"x":723,"y":136,"w":14,"h":65,"character":161,"shift":15,"offset":0,},
    "162": {"x":246,"y":69,"w":25,"h":65,"character":162,"shift":27,"offset":1,},
    "163": {"x":706,"y":2,"w":29,"h":65,"character":163,"shift":29,"offset":0,},
    "164": {"x":737,"y":2,"w":34,"h":65,"character":164,"shift":36,"offset":1,},
    "165": {"x":773,"y":2,"w":33,"h":65,"character":165,"shift":35,"offset":2,},
    "166": {"x":808,"y":2,"w":12,"h":65,"character":166,"shift":15,"offset":1,},
    "167": {"x":822,"y":2,"w":26,"h":65,"character":167,"shift":27,"offset":0,},
    "168": {"x":850,"y":2,"w":21,"h":65,"character":168,"shift":20,"offset":0,},
    "169": {"x":873,"y":2,"w":39,"h":65,"character":169,"shift":43,"offset":2,},
    "170": {"x":914,"y":2,"w":18,"h":65,"character":170,"shift":21,"offset":2,},
    "171": {"x":934,"y":2,"w":33,"h":65,"character":171,"shift":35,"offset":1,},
    "172": {"x":983,"y":2,"w":29,"h":65,"character":172,"shift":32,"offset":1,},
    "173": {"x":224,"y":69,"w":20,"h":65,"character":173,"shift":24,"offset":2,},
    "174": {"x":2,"y":69,"w":39,"h":65,"character":174,"shift":43,"offset":2,},
    "175": {"x":43,"y":69,"w":20,"h":65,"character":175,"shift":20,"offset":0,},
    "176": {"x":65,"y":69,"w":18,"h":65,"character":176,"shift":20,"offset":2,},
    "177": {"x":85,"y":69,"w":26,"h":65,"character":177,"shift":27,"offset":0,},
    "178": {"x":113,"y":69,"w":16,"h":65,"character":178,"shift":17,"offset":1,},
    "179": {"x":131,"y":69,"w":15,"h":65,"character":179,"shift":16,"offset":1,},
    "180": {"x":148,"y":69,"w":14,"h":65,"character":180,"shift":14,"offset":0,},
    "181": {"x":164,"y":69,"w":29,"h":65,"character":181,"shift":32,"offset":1,},
    "182": {"x":195,"y":69,"w":27,"h":65,"character":182,"shift":31,"offset":2,},
    "183": {"x":694,"y":2,"w":10,"h":65,"character":183,"shift":14,"offset":2,},
    "184": {"x":969,"y":2,"w":12,"h":65,"character":184,"shift":12,"offset":0,},
    "185": {"x":682,"y":2,"w":10,"h":65,"character":185,"shift":11,"offset":1,},
    "186": {"x":322,"y":2,"w":19,"h":65,"character":186,"shift":21,"offset":2,},
    "187": {"x":15,"y":2,"w":33,"h":65,"character":187,"shift":35,"offset":1,},
    "188": {"x":50,"y":2,"w":33,"h":65,"character":188,"shift":37,"offset":2,},
    "189": {"x":85,"y":2,"w":33,"h":65,"character":189,"shift":37,"offset":2,},
    "190": {"x":120,"y":2,"w":36,"h":65,"character":190,"shift":39,"offset":1,},
    "191": {"x":158,"y":2,"w":26,"h":65,"character":191,"shift":28,"offset":0,},
    "192": {"x":186,"y":2,"w":32,"h":65,"character":192,"shift":33,"offset":0,},
    "193": {"x":220,"y":2,"w":32,"h":65,"character":193,"shift":33,"offset":0,},
    "194": {"x":254,"y":2,"w":32,"h":65,"character":194,"shift":33,"offset":0,},
    "195": {"x":288,"y":2,"w":32,"h":65,"character":195,"shift":33,"offset":0,},
    "196": {"x":343,"y":2,"w":32,"h":65,"character":196,"shift":33,"offset":0,},
    "197": {"x":625,"y":2,"w":32,"h":65,"character":197,"shift":33,"offset":0,},
    "198": {"x":377,"y":2,"w":44,"h":65,"character":198,"shift":43,"offset":-1,},
    "199": {"x":423,"y":2,"w":31,"h":65,"character":199,"shift":33,"offset":2,},
    "200": {"x":456,"y":2,"w":24,"h":65,"character":200,"shift":27,"offset":2,},
    "201": {"x":482,"y":2,"w":26,"h":65,"character":201,"shift":27,"offset":2,},
    "202": {"x":510,"y":2,"w":24,"h":65,"character":202,"shift":27,"offset":2,},
    "203": {"x":536,"y":2,"w":24,"h":65,"character":203,"shift":27,"offset":2,},
    "204": {"x":562,"y":2,"w":16,"h":65,"character":204,"shift":17,"offset":-1,},
    "205": {"x":580,"y":2,"w":20,"h":65,"character":205,"shift":17,"offset":2,},
    "206": {"x":602,"y":2,"w":21,"h":65,"character":206,"shift":17,"offset":-1,},
    "207": {"x":659,"y":2,"w":21,"h":65,"character":207,"shift":17,"offset":-1,},
    "208": {"x":273,"y":69,"w":34,"h":65,"character":208,"shift":36,"offset":0,},
    "209": {"x":940,"y":69,"w":32,"h":65,"character":209,"shift":36,"offset":2,},
    "210": {"x":309,"y":69,"w":34,"h":65,"character":210,"shift":38,"offset":2,},
    "211": {"x":30,"y":136,"w":34,"h":65,"character":211,"shift":38,"offset":2,},
    "212": {"x":66,"y":136,"w":34,"h":65,"character":212,"shift":38,"offset":2,},
    "213": {"x":102,"y":136,"w":34,"h":65,"character":213,"shift":38,"offset":2,},
    "214": {"x":138,"y":136,"w":34,"h":65,"character":214,"shift":38,"offset":2,},
    "215": {"x":174,"y":136,"w":25,"h":65,"character":215,"shift":26,"offset":1,},
    "216": {"x":201,"y":136,"w":34,"h":65,"character":216,"shift":38,"offset":2,},
    "217": {"x":237,"y":136,"w":30,"h":65,"character":217,"shift":35,"offset":3,},
    "218": {"x":269,"y":136,"w":30,"h":65,"character":218,"shift":35,"offset":3,},
    "219": {"x":301,"y":136,"w":30,"h":65,"character":219,"shift":35,"offset":3,},
    "220": {"x":362,"y":136,"w":30,"h":65,"character":220,"shift":35,"offset":3,},
    "221": {"x":689,"y":136,"w":32,"h":65,"character":221,"shift":32,"offset":1,},
    "222": {"x":394,"y":136,"w":29,"h":65,"character":222,"shift":32,"offset":2,},
    "223": {"x":425,"y":136,"w":31,"h":65,"character":223,"shift":34,"offset":2,},
    "224": {"x":458,"y":136,"w":29,"h":65,"character":224,"shift":30,"offset":0,},
    "225": {"x":489,"y":136,"w":29,"h":65,"character":225,"shift":30,"offset":0,},
    "226": {"x":520,"y":136,"w":29,"h":65,"character":226,"shift":30,"offset":0,},
    "227": {"x":551,"y":136,"w":29,"h":65,"character":227,"shift":30,"offset":0,},
    "228": {"x":582,"y":136,"w":29,"h":65,"character":228,"shift":30,"offset":0,},
    "229": {"x":613,"y":136,"w":29,"h":65,"character":229,"shift":30,"offset":0,},
    "230": {"x":644,"y":136,"w":43,"h":65,"character":230,"shift":44,"offset":0,},
    "231": {"x":2,"y":136,"w":26,"h":65,"character":231,"shift":27,"offset":1,},
    "232": {"x":333,"y":136,"w":27,"h":65,"character":232,"shift":28,"offset":1,},
    "233": {"x":974,"y":69,"w":27,"h":65,"character":233,"shift":28,"offset":1,},
    "234": {"x":578,"y":69,"w":27,"h":65,"character":234,"shift":28,"offset":1,},
    "235": {"x":345,"y":69,"w":27,"h":65,"character":235,"shift":28,"offset":1,},
    "236": {"x":374,"y":69,"w":13,"h":65,"character":236,"shift":15,"offset":0,},
    "237": {"x":389,"y":69,"w":16,"h":65,"character":237,"shift":15,"offset":2,},
    "238": {"x":407,"y":69,"w":19,"h":65,"character":238,"shift":15,"offset":-2,},
    "239": {"x":428,"y":69,"w":21,"h":65,"character":239,"shift":15,"offset":-1,},
    "240": {"x":451,"y":69,"w":32,"h":65,"character":240,"shift":32,"offset":0,},
    "241": {"x":485,"y":69,"w":29,"h":65,"character":241,"shift":33,"offset":2,},
    "242": {"x":516,"y":69,"w":29,"h":65,"character":242,"shift":32,"offset":1,},
    "243": {"x":547,"y":69,"w":29,"h":65,"character":243,"shift":32,"offset":1,},
    "244": {"x":607,"y":69,"w":29,"h":65,"character":244,"shift":32,"offset":1,},
    "245": {"x":909,"y":69,"w":29,"h":65,"character":245,"shift":32,"offset":1,},
    "246": {"x":638,"y":69,"w":29,"h":65,"character":246,"shift":32,"offset":1,},
    "247": {"x":669,"y":69,"w":24,"h":65,"character":247,"shift":26,"offset":1,},
    "248": {"x":695,"y":69,"w":29,"h":65,"character":248,"shift":32,"offset":1,},
    "249": {"x":726,"y":69,"w":28,"h":65,"character":249,"shift":32,"offset":2,},
    "250": {"x":756,"y":69,"w":28,"h":65,"character":250,"shift":32,"offset":2,},
    "251": {"x":786,"y":69,"w":28,"h":65,"character":251,"shift":32,"offset":2,},
    "252": {"x":816,"y":69,"w":28,"h":65,"character":252,"shift":32,"offset":2,},
    "253": {"x":846,"y":69,"w":29,"h":65,"character":253,"shift":32,"offset":1,},
    "254": {"x":877,"y":69,"w":30,"h":65,"character":254,"shift":32,"offset":1,},
    "255": {"x":375,"y":337,"w":29,"h":65,"character":255,"shift":32,"offset":1,},
    "9647": {"x":406,"y":337,"w":31,"h":65,"character":9647,"shift":51,"offset":10,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":255,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_Details",
  "tags": [],
  "resourceType": "GMFont",
}