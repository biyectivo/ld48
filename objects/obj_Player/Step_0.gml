/// @description 
if (!Game.paused) {
	event_inherited();

	
	// Light
	with (helmet_light) {
		light.x = other.x;
		light.y = other.y;	
	}

	if (Game.current_depth > 0 && hp>0 && device_mouse_check_button_pressed(0,mb_right)) {
		if (helmet_light_battery > 0) {
			with (helmet_light) {
				light.visible = !light.visible;
			}
		}
	}
	else if (Game.current_depth == 0 && helmet_light.light.visible) {
		helmet_light.light.visible = false;	
	}

	if (helmet_light.light.visible) {
		helmet_light_battery = helmet_light_battery-LIGHT_BATTERY_RUNDOWN;
		if (helmet_light_battery < 10) {
			var _rnd = random_range(0.25,1.5);
			helmet_light.light.xscale = current_light_size * (1-(10-helmet_light_battery)*helmet_low_battery_penalty) + _rnd;
			helmet_light.light.yscale = current_light_size * (1-(10-helmet_light_battery)*helmet_low_battery_penalty) + _rnd;
		}
		if (helmet_light_battery <= 0) {
			helmet_light.light.visible = false;
		}
	}
	
	// Health
	if (Game.current_depth > 0) {
		if (helmet_light_battery == 0) {
			hp = max(0, hp-LOSE_HP_DARK*Game.current_depth/4);
		}
		else {
			hp = max(0, hp-LOSE_HP_NORMAL*Game.current_depth/4);
		}
	}
	
}

// Update shadows
/*
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;
*/