/// @description Create player

event_inherited(); // Agent
name = "Player";

// HP
max_hp = 100;
hp = max_hp;

// Animation	
animation_startindices = ds_map_create();
animation_lengths = ds_map_create();
animation_spacings = ds_map_create();
animation_speeds = ds_map_create();

// Beginning sprite frame index for each state
animation_startindices[? "Idle"] = 0;
animation_startindices[? "Walk"] = 4;
animation_startindices[? "Jump"] = 0;
animation_startindices[? "Mine"] = 36;
animation_startindices[? "Die"] = 54;

// Length in frames of animation for each state
animation_lengths[? "Idle"] = 1;
animation_lengths[? "Walk"] = 8;
animation_lengths[? "Jump"] = 0;
animation_lengths[? "Mine"] = 9;
animation_lengths[? "Die"] = 1;

// Spacing between frames of animation for each state (default = 1)
animation_spacings[? "Idle"] = 1;
animation_spacings[? "Walk"] = 1;
animation_spacings[? "Jump"] = 1;
animation_spacings[? "Mine"] = 1;
animation_spacings[? "Die"] = 1;

// Animation speed for each frame - lower number means higher speed
animation_speeds[? "Idle"] = 30;
animation_speeds[? "Walk"] = 6;
animation_speeds[? "Jump"] = 30;
animation_speeds[? "Mine"] = 2;
animation_speeds[? "Die"] = 30;

still_mining = false;


// Helmet light

helmet_light = instance_create_layer(x,y, layer_get_id("lyr_Light"), obj_StaticLight);
helmet_light_battery = 100;

helmet_light_level = 0;
current_light_size = Game.headlight_level_strength[helmet_light_level];
helmet_low_battery_penalty = 0.05;

with (helmet_light) {
	light.blend = Game.headlight_level_color[other.helmet_light_level];
	light.xscale = other.current_light_size;
	light.yscale = other.current_light_size;
	light.penumbraSize = 32;
	light.sprite = sLight32;
	light.x = other.x;
	light.y = other.y;
	light.visible = false;
}

// Bag
bag_level = 0;
bag_max_weight = Game.bag_level_capacity[bag_level];
bag_current_weight = 0;

// Pickaxe
pickaxe_level = 0;
pickaxe_max_health = Game.pickaxe_level_durability[pickaxe_level];
pickaxe_health = pickaxe_max_health;
pickaxe_damage = Game.pickaxe_level_strength[pickaxe_level];

// Shadows

/*
occluder       = new BulbDynamicOccluder(obj_BulbRenderer.lighting);
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;

var _l = -0.5*sprite_get_width(sprite_index);
var _t = -0.5*sprite_get_height(sprite_index);
var _r =  0.5*sprite_get_width(sprite_index);
var _b =  0.5*sprite_get_height(sprite_index);

//Use clockwise definitions!
occluder.AddEdge(_l, _t, _r, _t); //Top
occluder.AddEdge(_r, _t, _r, _b); //Right
occluder.AddEdge(_r, _b, _l, _b); //Bottom
occluder.AddEdge(_l, _b, _l, _t); //Left
*/