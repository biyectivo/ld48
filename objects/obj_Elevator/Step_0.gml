/// @description 
if (!Game.paused) {
	var _top = y;
	var _bottom = y+sprite_height;	
	on_base = (position_meeting(x, _bottom, obj_ElevatorBase) && !position_meeting(x, _bottom+1, obj_ElevatorBase));
	on_top = (position_meeting(x, _top, obj_ElevatorShaft) && !position_meeting(x, _top-1, obj_ElevatorShaft));
	player_on_elevator = obj_Player.bbox_left > bbox_left && obj_Player.bbox_right < bbox_right && obj_Player.bbox_bottom == bbox_bottom-1;
	player_input = (on_top && keyboard_check_pressed(Game.controls[? "down"])) || (on_base && keyboard_check_pressed(Game.controls[? "jump"]));
	
	is_moving = !(on_top || on_base);
	move = !is_moving && player_on_elevator && player_input;
	
	//show_debug_message("Move: "+string(move)+" is moving "+string(is_moving)+" "+string(player_on_elevator));
	
	if (move) {
		var _sign = on_top ? 1 : -1;
		current_speed = _sign * elevator_speed;
	}
	else if (!is_moving) {
		current_speed = 0;
	}
	
	y = y + current_speed;
	if (current_speed < 0 && player_on_elevator) {
		obj_Player.y = obj_Player.y + current_speed;
	}
}