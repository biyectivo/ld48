//*****************************************************************************
// Handle camera
//*****************************************************************************


// Fullscreen has changed (either via ALT+TAB or F10 on HTML5 or via menu)
if (fullscreen_change) {
	window_set_fullscreen(option_value[? "Fullscreen"]);
	fullscreen_change = false;	
	// Update graphics
	fnc_SetGraphics();
	
}
else if (keyboard_check_pressed(vk_f10)) {
	option_value[? "Fullscreen"] = window_get_fullscreen();	
	// Update graphics
	fnc_SetGraphics();
}

// GUI Size
if (window_get_fullscreen()) {
	display_set_gui_size(display_get_width(), display_get_height());
	//show_debug_message("Full screen: GUI is now "+string(display_get_gui_width())+"x"+string(display_get_gui_height()));
	//display_set_gui_maximize(1, 1, 0, 0);
}
else {		
	display_set_gui_size(window_get_width(), window_get_height());
	//show_debug_message("Windowed: GUI is now "+string(display_get_gui_width())+"x"+string(display_get_gui_height()));
	//show_debug_message(string(window_get_width())+" "+string(window_get_height()));
}

if (!Game.paused) {	
	
	if (room == room_Game_1) {
	
		if (instance_exists(camera_target)) {
			
			// Get current camera position
			var _current_camera_x = camera_get_view_x(VIEW);
			var _current_camera_y = camera_get_view_y(VIEW);
	
			// Calculate offset for screen shake			
			var _offsetx = 0;
			var _offsety = 0;
			if (camera_shake) {
				var _offsetx = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
				var _offsety = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
			}
			
			var _previous_cursor = cursor_sprite;
			if (mouse_check_button(mb_middle)) {
				cursor_sprite = spr_Cursor_Pan;
				var _destx = clamp(device_mouse_x(0)-adjusted_camera_width/2, 0, room_width-adjusted_camera_width);
				var _desty = clamp(device_mouse_y(0)-adjusted_camera_height/2, 0, room_height-adjusted_camera_height);
			}
			else {
				cursor_sprite = _previous_cursor;
				// Get target camera position, centered around the target and lerped with camera smoothness
				var _destx = clamp(camera_target.x-adjusted_camera_width/2, 0, room_width-adjusted_camera_width);
				var _desty = clamp(camera_target.y-adjusted_camera_height/2, 0, room_height-adjusted_camera_height);
			}
			
			var _target_camera_x = lerp(_current_camera_x, _destx, camera_smoothness) + _offsetx;
			var _target_camera_y = lerp(_current_camera_y, _desty, camera_smoothness) + _offsety;
			
			// Move camera
			camera_set_view_pos(VIEW, _target_camera_x, _target_camera_y);
			
		}
		
		// Performance enhancement post-jam: 20210426		
		instance_activate_all();
		var _margin = 2*TILE_SIZE;
		var _block;
		with (obj_Player) {
			_block = instance_place(x, y+1, obj_Block);						
		}
		
		instance_deactivate_region(	clamp(camera_get_view_x(VIEW)-_margin, 0, room_width), 
									clamp(camera_get_view_y(VIEW)-_margin, 0, room_height),
									clamp(camera_get_view_x(VIEW)+camera_get_view_width(VIEW)+_margin, 0, room_width),
									clamp(camera_get_view_y(VIEW)+camera_get_view_height(VIEW)+_margin, 0, room_height),
									false,
									true);
		
		// Activate block just below player, so it doesn't fall - especially for camera panning				
		instance_activate_object(_block);		
		instance_activate_object(obj_Elevator);
		instance_activate_object(obj_ElevatorBase);
		instance_activate_object(obj_ElevatorShaft);
		instance_activate_object(obj_EndOfWorld);
		instance_activate_object(cls_Transition);
		instance_activate_object(obj_BulbRenderer);		
		instance_activate_object(obj_Jewel);
		instance_activate_object(obj_Shop);
		instance_activate_object(obj_Player);
		instance_activate_object(obj_Player.helmet_light);
		instance_activate_object(obj_gmlive);
		
		//show_debug_message(string(array_length(global.TypeFormatted_elementArray))+" "+string(array_length(global.TypeFormatted_stringArray)));
		
		// Skip tutorial
		if (device_mouse_check_button_pressed(0, mb_right) && tutorial_index < array_length(tutorial)) {
			alarm[4] = -1;
			show_tutorial_message = false;
			tutorial_index = array_length(tutorial);
		}
		
		
	}
	
}