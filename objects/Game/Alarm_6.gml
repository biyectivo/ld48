/// @description Rescue/end-game transition
if (rescue_transition_type != 0) {
	
	rescue_transition_type  = (rescue_transition_type  + 1) % 3;
	alarm[6] = RESCUE_TRANSITION_TIME;
	if (rescue_transition_type == 2) {
		if (obj_Player.hp > 0) {
			obj_Player.x = obj_Player.xstart;
			obj_Player.y = obj_Player.ystart;
			obj_Player.controllable = true;
			fnc_Dispose();	
			// Move elevator
			var _num_cols = ceil(room_width/TILE_SIZE);
			obj_Elevator.x = floor(_num_cols/2)*TILE_SIZE;
			obj_Elevator.y = 8+7*TILE_SIZE;
			num_rescues++;
		}
		else {
			/// Lost
			Game.lost = true;
			Game.paused = true;	
		}
	}
}