

// Mouse cursor
window_set_cursor(cr_none);


// Avoid automatic drawing of the application surface in order to draw it manually using the desired game resolution (independent of the window resolution)
//application_surface_draw_enable(false);

// Go to next room
room_goto(room_UI_Title);
