if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if (Game.lost && Game.paused && ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter)))) {
	room_restart();
}

// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
		if (instance_exists(obj_Player)) {
			current_step++;
		}
		
		// Change cursor
		if ( (	(device_mouse_x(0) > obj_Player.bbox_right && device_mouse_x(0) < obj_Player.bbox_right + TILE_SIZE && device_mouse_y(0) >= obj_Player.y - obj_Player.sprite_height/2 && device_mouse_y(0) <= obj_Player.y + obj_Player.sprite_height/2) ||
				(device_mouse_x(0) < obj_Player.bbox_left && device_mouse_x(0) > obj_Player.bbox_left - TILE_SIZE && device_mouse_y(0) >= obj_Player.y - obj_Player.sprite_height/2 && device_mouse_y(0) <= obj_Player.y + obj_Player.sprite_height/2) ||
				(device_mouse_y(0) > obj_Player.bbox_bottom && device_mouse_y(0) < obj_Player.bbox_bottom + TILE_SIZE && device_mouse_x(0) >= obj_Player.x - obj_Player.sprite_width/2 && device_mouse_x(0) <= obj_Player.x + obj_Player.sprite_width/2) ||
				// Mine block above you, introduced in 1.2.0
				(device_mouse_y(0) < obj_Player.bbox_top && device_mouse_y(0) > obj_Player.bbox_top - TILE_SIZE && device_mouse_x(0) >= obj_Player.x - obj_Player.sprite_width/2 && device_mouse_x(0) <= obj_Player.x + obj_Player.sprite_width/2)
			)
			&& position_meeting(device_mouse_x(0), device_mouse_y(0), obj_EarthBlock)) {
			if (obj_Player.pickaxe_health > 0) {
				cursor_sprite = spr_Cursor_Pickaxe;
			}
			else {
				cursor_sprite = spr_Cursor_Pickaxe_Nope;
			}
		}
		else {			
			cursor_sprite = spr_Cursor_Mouse;
		}
		
		// Calculate depth
		current_depth = max(floor(obj_Player.y / TILE_SIZE)-8, 0);
		if (current_depth > max_depth) {
			max_depth = current_depth;
		}
		
		// Music
		if (current_depth >= 4) {
			audio_sound_gain(music_sound_id_inside, 1,0);
			audio_sound_gain(music_sound_id_outside, 0,0);	
		}
		else if (current_depth == 0) {
			audio_sound_gain(music_sound_id_inside, 0,0);
			audio_sound_gain(music_sound_id_outside, 1,0);
		}
		else {
			audio_sound_gain(music_sound_id_inside, current_depth/4,0);
			audio_sound_gain(music_sound_id_outside, 1-current_depth/4,0);
		}
		
	}
}
