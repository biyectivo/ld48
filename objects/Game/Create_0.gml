//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************

randomize();

//*****************************************************************************
// Set camera target
//*****************************************************************************

camera_target = obj_Player;
camera_shake = false;
camera_smoothness = 0.1;

//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(4);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
controls = ds_map_create();
control_names = ds_map_create();

control_indices[0] = "left";
control_indices[1] = "right";
control_indices[2] = "jump";
control_indices[3] = "down";

controls[? "left"] = ord("A"); //vk_left;
controls[? "right"] = ord("D"); //vk_right;
controls[? "jump"] = ord("W"); //vk_up;
controls[? "down"] = ord("S");


control_names[? "left"] = "Move Left";
control_names[? "right"] = "Move Right";
control_names[? "jump"] = "Jump/Up";
control_names[? "down"] = "Down";

name_being_modified = false;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");

credits[0] = "[fnt_Details][scale,0.7][fa_middle][fa_center][c_white]2021 by biyectivo#2771";
credits[1] = "[fnt_Details][scale,0.7][fa_middle][fa_center][c_white](José Alberto Bonilla Vera)";
credits[2] = "[fnt_Details][scale,0.7][fa_middle][fa_center][c_white]Made for the Ludum Dare 48 Compo";
credits[3] = "[fnt_Details][scale,0.7][fa_middle][fa_center][c_white]version "+string(VERSION);

game_title = "Shiny [scale,3][spr_Jewels,25][scale,1] Depths";
scoreboard_game_id = "Shiny Depths";

primary_gamepad = -1;
start_drag_drop = false;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter = noone;

pause_screenshot = noone;

//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;

fnc_InitializeGameStartVariables();
