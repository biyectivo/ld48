/// @description
if (!Game.paused) {
	var _weight = Game.jewels[image_index].weight;
	if (obj_Player.bag_current_weight + _weight <= obj_Player.bag_max_weight) {
		obj_Player.bag_current_weight = obj_Player.bag_current_weight + _weight ;
		Game.jewels[image_index].currently_have++;
		Game.jewels[image_index].total_had++;
		
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(choose(snd_Jewel_1, snd_Jewel_2, snd_Jewel_3), 1,false);
		}
		instance_destroy();
	}
}