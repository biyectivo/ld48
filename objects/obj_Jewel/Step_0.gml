/// @description 
if (!Game.paused) {
	visible = (Game.current_depth > 0 && distance_to_object(obj_Player) < (obj_Player.current_light_size)/2 * TILE_SIZE && obj_Player.helmet_light.light.visible);
	
	
	if (distance_to_object(obj_Player) < 2*TILE_SIZE && !place_meeting(x, y, obj_EarthBlock)) {
		current_vspeed = current_vspeed + GRAVITY;
	
		// Check for collisions with collisionable objects and snap to the bounding box if needed
		if (place_meeting(x, y + current_vspeed, cls_Collisionable)) {
			var _id = instance_place(x, y + current_vspeed, cls_Collisionable);			
			if (object_get_name(_id.object_index) != "obj_Player") {				
				while (!place_meeting(x, y + sign(current_vspeed), cls_Collisionable)) {
					y = y + sign(current_vspeed);
				}
				current_vspeed = 0;
			}
			else {
				event_perform(ev_collision, obj_Player);
			}
		}
		else {
			y = y + current_vspeed;
		}
	}
	
}