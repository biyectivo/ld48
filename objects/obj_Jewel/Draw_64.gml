/// @description 
if (!Game.paused) {
	fnc_BackupDrawParams();
	
	if (position_meeting(device_mouse_x(0), device_mouse_y(0), self)) { 
		// Name
		var _string = "[fnt_Details][fa_middle][fa_center][scale,0.4][c_white]"+Game.jewels[image_index].name;
	
		var _gui_x = device_mouse_x_to_gui(0);
		var _gui_y = device_mouse_y_to_gui(0)-3*TILE_SIZE;
	
		var _text = type_formatted(_gui_x, _gui_y, _string, false);
		var _x11 = _gui_x-_text.bbox_width/2-10;
		var _x21 = _gui_x+_text.bbox_width/2+10;
		var _y11 = _gui_y-_text.bbox_height/2-10;
		var _y21 = _gui_y+_text.bbox_height/2+10;
		
		
		// Description
		var _string2 = "[fnt_Details][fa_middle][fa_center][scale,0.22][c_white]"+Game.jewels[image_index].description;
		
		var _gui_x2 = _gui_x;
		var _gui_y2 = _gui_y + _text.bbox_height;
		
		var _text2 = type_formatted(_gui_x2, _gui_y2, _string2, false);		
		var _x12 = _gui_x2-_text2.bbox_width/2-10;
		var _x22 = _gui_x2+_text2.bbox_width/2+10;
		var _y12 = _gui_y2-_text2.bbox_height/2-10;
		var _y22 = _gui_y2+_text2.bbox_height/2+10;
		
		
		// Weight
		if (Game.jewels[image_index].weight > obj_Player.bag_max_weight-obj_Player.bag_current_weight) {
			var _color = "[c_red]";
		}
		else {
			var _color = "[c_white]";
		}
		var _string3 = "[fnt_Details][fa_middle][fa_center][scale,0.25][c_white]Weight: "+_color+string(Game.jewels[image_index].weight);
		
		var _gui_x3 = _gui_x2;
		var _gui_y3 = _gui_y2 + _text2.bbox_height;
		
		var _text3 = type_formatted(_gui_x3, _gui_y3, _string3, false);		
		var _x13 = _gui_x3-_text3.bbox_width/2-10;
		var _x23 = _gui_x3+_text3.bbox_width/2+10;
		var _y13 = _gui_y3-_text3.bbox_height/2-10;
		var _y23 = _gui_y3+_text3.bbox_height/2+10;
		
		
		// Value
		var _string4 = "[fnt_Details][fa_middle][fa_center][scale,0.25][c_white]Value: $"+fnc_FormatString(string(Game.jewels[image_index].value));
	
		var _gui_x4 = _gui_x3;
		var _gui_y4 = _gui_y3 + _text3.bbox_height;
		
		var _text4 = type_formatted(_gui_x4, _gui_y4, _string4, false);		
		var _x14 = _gui_x4-_text4.bbox_width/2-10;
		var _x24 = _gui_x4+_text4.bbox_width/2+10;
		var _y14 = _gui_y4-_text4.bbox_height/2-10;
		var _y24 = _gui_y4+_text4.bbox_height/2+10;
		
		
		// Draw tooltip
		draw_set_alpha(0.6);		
		draw_roundrect_color_ext(min(_x11,_x12, _x13, _x14), min(_y11,_y12, _y13, _y14), max(_x21,_x22,_x23, _x24), max(_y21,_y22,_y23, _y24), 15, 15, c_black, c_black, false);
		draw_set_alpha(1);
		
		type_formatted(_gui_x, _gui_y, _string);		
		type_formatted(_gui_x2, _gui_y2, _string2);
		type_formatted(_gui_x3, _gui_y3, _string3);
		type_formatted(_gui_x4, _gui_y4, _string4);
	}
	fnc_RestoreDrawParams();
}