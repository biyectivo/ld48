/// @description Particles when you mine

particle_type_rubble = part_type_create();		
part_type_scale(particle_type_rubble, 1, 1);
part_type_size(particle_type_rubble, 1.8, 2, -0.01, 0);
part_type_life(particle_type_rubble, 10, 20);			
part_type_shape(particle_type_rubble, pt_shape_pixel);				
part_type_alpha1(particle_type_rubble, 1);
part_type_speed(particle_type_rubble, 1.5, 2, 0, 0.5);

if (mining_direction == "up") {	
	var _min_direction = 200;
	var _max_direction = 340;
	var _region_x_min = x;
	var _region_x_max = x+TILE_SIZE;
	var _region_y_min = y+TILE_SIZE/2;
	var _region_y_max = y+TILE_SIZE;
}
else if (mining_direction == "down") {
	var _min_direction = 20;
	var _max_direction = 160;
	var _region_x_min = x;
	var _region_x_max = x+TILE_SIZE;
	var _region_y_min = y;
	var _region_y_max = y+TILE_SIZE/2;
}
else if (mining_direction == "left") {
	var _min_direction = -70;
	var _max_direction = 70;
	var _region_x_min = x+TILE_SIZE/2;
	var _region_x_max = x+TILE_SIZE;
	var _region_y_min = y;
	var _region_y_max = y+TILE_SIZE;
}
else { // right
	var _min_direction = 110;
	var _max_direction = 250;
	var _region_x_min = x;
	var _region_x_max = x+TILE_SIZE/2;
	var _region_y_min = y;
	var _region_y_max = y+TILE_SIZE;
}

part_type_direction(particle_type_rubble, _min_direction, _max_direction, 0, 10);

part_type_orientation(particle_type_rubble, -10, 10, 0, 0, false);
part_type_color1(particle_type_rubble, $1B3343);
part_type_blend(particle_type_rubble, false);

part_emitter_region(Game.particle_system, Game.particle_emitter, _region_x_min, _region_x_max, _region_y_min, _region_y_max, ps_shape_rectangle, ps_distr_gaussian);
part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_rubble, 20);
