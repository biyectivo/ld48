/// @description 
if (!Game.paused) {
	// Increase hardness if jewel is there
	if (not_increased_hp) {
		if (place_meeting(x, y, obj_Jewel)) {
			var _id = instance_place(x, y, obj_Jewel);
			max_hp = max_hp + Game.jewels[_id.image_index].increase_block_strength; 	
			hp = max_hp;
		}
		not_increased_hp = false;
	}
	
	// Mine
	if (hp<=0 && !obj_Player.still_mining) {
		instance_destroy();
	}
	
	
}