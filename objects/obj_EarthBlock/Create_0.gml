/// @description 
sprite_index = spr_EarthBlocks;
image_index = irandom_range(0, sprite_get_number(spr_EarthBlocks)-1);
max_hp = irandom_range(2,3);
hp = max_hp;
block_depth = 0;

not_increased_hp = true;

random_crack = irandom_range(0, sprite_get_number(spr_Cracks)/2-1);

initialized = true;