/// @description 
// Draw dust
particle_type_dust = part_type_create();		
part_type_scale(particle_type_dust, 1, 1);
part_type_size(particle_type_dust, 0.8, 1.5, 0, 0);
part_type_life(particle_type_dust, 60, 120);			
part_type_shape(particle_type_dust, pt_shape_cloud);				
part_type_alpha2(particle_type_dust, 0.5, 0.0);
part_type_speed(particle_type_dust, 0.1, 0.1, 0, 0);
part_type_direction(particle_type_dust, 0, 360, 0, 20);
part_type_orientation(particle_type_dust, -10, 10, 0, 0, false);
part_type_color1(particle_type_dust, $1B3343);
part_type_blend(particle_type_dust, false);

part_emitter_region(Game.particle_system, Game.particle_emitter, x, x+TILE_SIZE, y, y+TILE_SIZE, ps_shape_rectangle, ps_distr_gaussian);
part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_dust, 40);
