/// @description 
if (!Game.paused) {
	fnc_BackupDrawParams();
	
	if (position_meeting(device_mouse_x(0), device_mouse_y(0), self)) { 
		
		if (distance_to_object(obj_Player) < TILE_SIZE) {
			var _color = "[c_white]";
		}
		else {
			var _color = "[$aaaaaa]";
		}
		var _string = "[fnt_Details][fa_middle][fa_center][scale,0.4]"+_color+"Shop";
	
		var _gui_x = device_mouse_x_to_gui(0);
		var _gui_y = device_mouse_y_to_gui(0)-2*TILE_SIZE;
	
		var _text = type_formatted(_gui_x, _gui_y, _string, false);
		draw_set_alpha(0.6);
		var _x1 = _gui_x-_text.bbox_width/2-10;
		var _x2 = _gui_x+_text.bbox_width/2+10;
		var _y1 = _gui_y-_text.bbox_height/2-10;
		var _y2 = _gui_y+_text.bbox_height/2+10;
		draw_roundrect_color_ext(_x1, _y1, _x2, _y2, 15, 15, c_black, c_black, false);
		draw_set_alpha(1);
		type_formatted(_gui_x, _gui_y, _string);
	}
	fnc_RestoreDrawParams();
}