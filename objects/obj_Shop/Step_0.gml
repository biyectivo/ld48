/// @description 
if (!Game.paused && !Game.draw_shop) {
	if (distance_to_object(obj_Player) < TILE_SIZE && device_mouse_check_button_pressed(0,mb_left) && position_meeting(device_mouse_x(0), device_mouse_y(0), self)) {
		Game.draw_shop = true;
		obj_Player.controllable = false;
	}
}