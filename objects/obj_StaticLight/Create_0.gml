light = new BulbLight(obj_BulbRenderer.lighting, light_mask, 0, x, y);
with (light) {
	penumbraSize = other.penumbra_size;
	xscale = other.x_scale;
	yscale = other.y_scale;
	blend = other.ambient_light_color;
}