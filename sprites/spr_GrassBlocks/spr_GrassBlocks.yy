{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6a12418e-f653-4225-97f6-9d8ae1376f9f","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6a12418e-f653-4225-97f6-9d8ae1376f9f","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"6a12418e-f653-4225-97f6-9d8ae1376f9f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9aa3cbe9-5ba9-42b6-b720-59dd90d58cee","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9aa3cbe9-5ba9-42b6-b720-59dd90d58cee","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"9aa3cbe9-5ba9-42b6-b720-59dd90d58cee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"033a89d5-89b1-4627-bd78-279527af37b3","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"033a89d5-89b1-4627-bd78-279527af37b3","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"033a89d5-89b1-4627-bd78-279527af37b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9fbbba93-93c0-4100-ad05-b05a2f722733","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9fbbba93-93c0-4100-ad05-b05a2f722733","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"9fbbba93-93c0-4100-ad05-b05a2f722733","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"974ee727-aff8-49cc-b152-bc72de82f109","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"974ee727-aff8-49cc-b152-bc72de82f109","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"974ee727-aff8-49cc-b152-bc72de82f109","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a26a6274-0230-4bc7-9f4c-c1cde38cb3ea","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a26a6274-0230-4bc7-9f4c-c1cde38cb3ea","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"a26a6274-0230-4bc7-9f4c-c1cde38cb3ea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0084cd77-1d36-468b-a618-554d4602a561","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0084cd77-1d36-468b-a618-554d4602a561","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"0084cd77-1d36-468b-a618-554d4602a561","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"85f38cb0-1e0b-45f3-bf22-6803951983fc","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"85f38cb0-1e0b-45f3-bf22-6803951983fc","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"LayerId":{"name":"d839cfb4-23a4-4270-a2e7-267f45098743","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","name":"85f38cb0-1e0b-45f3-bf22-6803951983fc","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"621bd80d-a7ef-4c84-a32c-75dca289c3f6","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6a12418e-f653-4225-97f6-9d8ae1376f9f","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b5b40da-b086-4043-b92e-f6314161fcec","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9aa3cbe9-5ba9-42b6-b720-59dd90d58cee","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ebabada3-f213-4f63-8df4-e39a976a9d36","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"033a89d5-89b1-4627-bd78-279527af37b3","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e7f8ceb-880c-4c15-9e0c-be0671768bc3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9fbbba93-93c0-4100-ad05-b05a2f722733","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"99bc326f-08e3-444c-85b5-e681b049d022","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"974ee727-aff8-49cc-b152-bc72de82f109","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e02040ef-8d3c-421b-9201-bb27a6b2baf8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a26a6274-0230-4bc7-9f4c-c1cde38cb3ea","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18d5920a-11e8-4066-9510-028bb66588ff","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0084cd77-1d36-468b-a618-554d4602a561","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0bf033b6-5b68-45f0-9e9a-bba464fa39e6","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"85f38cb0-1e0b-45f3-bf22-6803951983fc","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GrassBlocks","path":"sprites/spr_GrassBlocks/spr_GrassBlocks.yy",},
    "resourceVersion": "1.3",
    "name": "spr_GrassBlocks",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d839cfb4-23a4-4270-a2e7-267f45098743","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GrassBlocks",
  "tags": [],
  "resourceType": "GMSprite",
}