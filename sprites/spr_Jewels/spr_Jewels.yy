{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 30,
  "bbox_top": 2,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c4a919c5-0f3f-4dc8-b472-a59ec4f65abe","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c4a919c5-0f3f-4dc8-b472-a59ec4f65abe","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"c4a919c5-0f3f-4dc8-b472-a59ec4f65abe","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"50c7ffe5-3d8a-4253-9d2e-7b56edbbe6f5","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"50c7ffe5-3d8a-4253-9d2e-7b56edbbe6f5","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"50c7ffe5-3d8a-4253-9d2e-7b56edbbe6f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"30ad610b-308f-4a83-ad39-be646b42eda3","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"30ad610b-308f-4a83-ad39-be646b42eda3","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"30ad610b-308f-4a83-ad39-be646b42eda3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a7c7f11f-591c-4458-81ba-65618897f73e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a7c7f11f-591c-4458-81ba-65618897f73e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"a7c7f11f-591c-4458-81ba-65618897f73e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c4d0521a-69b0-4198-a55b-e211afb7d62d","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c4d0521a-69b0-4198-a55b-e211afb7d62d","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"c4d0521a-69b0-4198-a55b-e211afb7d62d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b4e90318-2168-4a63-9c1b-ecf8bc871e42","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b4e90318-2168-4a63-9c1b-ecf8bc871e42","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"b4e90318-2168-4a63-9c1b-ecf8bc871e42","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"73047c1c-dc31-4d60-be00-5cd8fba33b5a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"73047c1c-dc31-4d60-be00-5cd8fba33b5a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"73047c1c-dc31-4d60-be00-5cd8fba33b5a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cea042fc-23dc-4710-99dc-2fc2f64901f5","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cea042fc-23dc-4710-99dc-2fc2f64901f5","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"cea042fc-23dc-4710-99dc-2fc2f64901f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1657871b-3e7f-41c3-b083-bd80899f8cb8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1657871b-3e7f-41c3-b083-bd80899f8cb8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"1657871b-3e7f-41c3-b083-bd80899f8cb8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b1904a73-6215-4c1a-b889-2d1fe0359ac0","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b1904a73-6215-4c1a-b889-2d1fe0359ac0","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"b1904a73-6215-4c1a-b889-2d1fe0359ac0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ddaeb45f-b13f-4947-b239-7ab200e80908","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ddaeb45f-b13f-4947-b239-7ab200e80908","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"ddaeb45f-b13f-4947-b239-7ab200e80908","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5b96b629-8fa3-4609-968e-aad62e78c95e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5b96b629-8fa3-4609-968e-aad62e78c95e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"5b96b629-8fa3-4609-968e-aad62e78c95e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3dd681a6-5362-4802-b3d1-d3d237cfa2cc","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3dd681a6-5362-4802-b3d1-d3d237cfa2cc","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"3dd681a6-5362-4802-b3d1-d3d237cfa2cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd5b9ad5-b3a4-4d70-b242-614b34e4866a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd5b9ad5-b3a4-4d70-b242-614b34e4866a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"dd5b9ad5-b3a4-4d70-b242-614b34e4866a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc9c48c8-2667-409e-bddf-ef8b428e42bd","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc9c48c8-2667-409e-bddf-ef8b428e42bd","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"fc9c48c8-2667-409e-bddf-ef8b428e42bd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1512a0bd-08a3-4103-9ee4-5b35ce7d1bc4","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1512a0bd-08a3-4103-9ee4-5b35ce7d1bc4","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"1512a0bd-08a3-4103-9ee4-5b35ce7d1bc4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f176470-318f-44de-ad3b-104236f4416a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f176470-318f-44de-ad3b-104236f4416a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"2f176470-318f-44de-ad3b-104236f4416a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0267ea0c-2132-4f53-95af-b26294793ea8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0267ea0c-2132-4f53-95af-b26294793ea8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"0267ea0c-2132-4f53-95af-b26294793ea8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"14b19277-c262-4600-947c-eea582a579b8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"14b19277-c262-4600-947c-eea582a579b8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"14b19277-c262-4600-947c-eea582a579b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1b344d25-b6ae-4026-bbc0-dc7f0332a1d7","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b344d25-b6ae-4026-bbc0-dc7f0332a1d7","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"1b344d25-b6ae-4026-bbc0-dc7f0332a1d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ef7394f4-3b8f-4638-83de-cb18ccb1541d","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ef7394f4-3b8f-4638-83de-cb18ccb1541d","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"ef7394f4-3b8f-4638-83de-cb18ccb1541d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"01257c0b-60f9-4312-856e-a0d74e77c8be","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"01257c0b-60f9-4312-856e-a0d74e77c8be","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"01257c0b-60f9-4312-856e-a0d74e77c8be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8af7317-074d-4959-8696-787931603941","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8af7317-074d-4959-8696-787931603941","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"a8af7317-074d-4959-8696-787931603941","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"756d56e1-b97d-4f4a-b78e-99cdfbb55e42","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"756d56e1-b97d-4f4a-b78e-99cdfbb55e42","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"756d56e1-b97d-4f4a-b78e-99cdfbb55e42","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f96584b-51b8-47d1-a7cb-300a0dc50d8e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f96584b-51b8-47d1-a7cb-300a0dc50d8e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"4f96584b-51b8-47d1-a7cb-300a0dc50d8e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"40015220-bb52-4d32-bf5d-3adbd79dbd11","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"40015220-bb52-4d32-bf5d-3adbd79dbd11","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"40015220-bb52-4d32-bf5d-3adbd79dbd11","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"125d39e2-8b6e-4eee-aabe-55b803216fe7","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"125d39e2-8b6e-4eee-aabe-55b803216fe7","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"125d39e2-8b6e-4eee-aabe-55b803216fe7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cf2c782c-87ac-4140-aee7-a8b2d03f1680","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cf2c782c-87ac-4140-aee7-a8b2d03f1680","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"cf2c782c-87ac-4140-aee7-a8b2d03f1680","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"455ad505-32c1-44ec-801d-d79072a931da","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"455ad505-32c1-44ec-801d-d79072a931da","path":"sprites/spr_Jewels/spr_Jewels.yy",},"LayerId":{"name":"c392d862-d7e3-4314-a8e9-37f0bd024193","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","name":"455ad505-32c1-44ec-801d-d79072a931da","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 29.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7fa0ad2e-3c9d-4437-ad1e-8f22cfa94c73","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c4a919c5-0f3f-4dc8-b472-a59ec4f65abe","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4520afdb-e528-4208-9bbd-97e41e9aa39b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"50c7ffe5-3d8a-4253-9d2e-7b56edbbe6f5","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0de5437c-6038-485c-9e8f-ccdc142a9194","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30ad610b-308f-4a83-ad39-be646b42eda3","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"86092ce3-3204-4d9b-9cda-a45b4b06fcb9","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a7c7f11f-591c-4458-81ba-65618897f73e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea1c3c53-ce34-4bf4-be2b-ac2f495e7627","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c4d0521a-69b0-4198-a55b-e211afb7d62d","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"51afa8bd-c49e-429f-8169-28f311b8ae59","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4e90318-2168-4a63-9c1b-ecf8bc871e42","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8853b573-4fa9-4194-9497-0691b85390ef","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"73047c1c-dc31-4d60-be00-5cd8fba33b5a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bb7f98ec-db29-4c09-8079-34b6eed9e607","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cea042fc-23dc-4710-99dc-2fc2f64901f5","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cff1e8da-dfea-4a87-9580-b833c83c82b7","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1657871b-3e7f-41c3-b083-bd80899f8cb8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ffbc900-80db-477e-ae68-79bca59d1c54","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b1904a73-6215-4c1a-b889-2d1fe0359ac0","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9f9afe32-6545-4972-ae32-b75a5605ee04","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ddaeb45f-b13f-4947-b239-7ab200e80908","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"696b9d39-1d9c-4577-b449-dd0b92668ca2","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5b96b629-8fa3-4609-968e-aad62e78c95e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea54fa9c-d161-4f7e-83d0-791f18e457bb","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3dd681a6-5362-4802-b3d1-d3d237cfa2cc","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da6b07f8-507d-4edc-9c74-4bf25023bf4f","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd5b9ad5-b3a4-4d70-b242-614b34e4866a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ffe953c7-7de7-4ef8-a851-25a0ba7a2bc0","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc9c48c8-2667-409e-bddf-ef8b428e42bd","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f5bba381-e882-4f1a-b3ea-09250ee61872","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1512a0bd-08a3-4103-9ee4-5b35ce7d1bc4","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"93e1e7e1-f97d-4e4a-be6b-328e305221f5","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f176470-318f-44de-ad3b-104236f4416a","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d93da27d-4835-47ff-a21b-6433389f3875","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0267ea0c-2132-4f53-95af-b26294793ea8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b10b4f53-4bbc-48f8-8808-a2a3a9bf469d","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14b19277-c262-4600-947c-eea582a579b8","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"19e8b9fc-5a10-4c54-864d-7cc59a5a8387","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b344d25-b6ae-4026-bbc0-dc7f0332a1d7","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"95f314d8-6d5f-48aa-af28-14506948e258","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ef7394f4-3b8f-4638-83de-cb18ccb1541d","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4896d43d-b493-44aa-abb5-28938c662165","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"01257c0b-60f9-4312-856e-a0d74e77c8be","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00c8dc2a-aad2-4c90-830a-8f5e699e5fbe","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8af7317-074d-4959-8696-787931603941","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"675a88cb-0a30-4e4a-b355-5a81298c641a","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"756d56e1-b97d-4f4a-b78e-99cdfbb55e42","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ce42343e-0abc-49cc-ba31-c6085617b52a","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f96584b-51b8-47d1-a7cb-300a0dc50d8e","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ce61cb7-330c-489d-80c5-502e00b22344","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"40015220-bb52-4d32-bf5d-3adbd79dbd11","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d98929bd-eb64-4b01-a8ef-b7c5c16517cd","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"125d39e2-8b6e-4eee-aabe-55b803216fe7","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"642cb58d-ebf1-4c20-8dfc-493f431fc013","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cf2c782c-87ac-4140-aee7-a8b2d03f1680","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"916a9f05-579c-4d2c-a67f-600e91bade34","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"455ad505-32c1-44ec-801d-d79072a931da","path":"sprites/spr_Jewels/spr_Jewels.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Jewels","path":"sprites/spr_Jewels/spr_Jewels.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Jewels",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c392d862-d7e3-4314-a8e9-37f0bd024193","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Jewels",
  "tags": [],
  "resourceType": "GMSprite",
}