
/// @function fnc_PlayerFSM_Idle
/// @description Idle state for player
function fnc_PlayerFSM_Idle() {

	// Gravity
	fnc_PlayerFSM_Gravity();

	// Get Input
	fnc_PlayerFSM_GetInput();
	
	
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}

function fnc_PlayerFSM_ProcessHorizontal(_commit_speed) {
	// Check for collisions with collisionable objects and snap to the bounding box if needed
	if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")),x + _commit_speed, y) != 1)  {	
		if (place_meeting(x + _commit_speed, y, cls_Collisionable)) {
			//var _id = instance_place(x + _commit_speed, y, cls_Collisionable);
			
			while (!place_meeting(x + sign(_commit_speed), y, cls_Collisionable)) {
				x = x + sign(_commit_speed);
			}
			
		}
		else {
			x = x + _commit_speed;
		}
	}
}

/// @function fnc_PlayerFSM_Walk
/// @description Walk state for player
function fnc_PlayerFSM_Walk() {
	
	// Apply Gravity
	fnc_PlayerFSM_Gravity();
	
	// Via keys - horizontal	
	var _commit_speed;		
	_commit_speed = player_horizontal_input * walk_hspeed;		
	fnc_PlayerFSM_ProcessHorizontal(_commit_speed);
	
	// Update facing
	if (player_horizontal_input != 0) {
		facing = (player_horizontal_input == 1 ? FACING.EAST : FACING.WEST);
	}
	
	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}


/// @function fnc_PlayerFSM_Jump
/// @description Jump state for player
function fnc_PlayerFSM_Jump() {
	
	
	// Via keys - vertical	
	if (fnc_PlayerFSM_OnGround()) {
		current_vspeed = -player_jump_input * jump_speed;
	}
	
	// Gravity
	fnc_PlayerFSM_Gravity();
	
	// Facing deleted
	
	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}


/// @function fnc_PlayerFSM_Gravity
/// @description Process gravity
function fnc_PlayerFSM_Gravity() {
	
	current_vspeed = current_vspeed + GRAVITY;
	
	// Check for collisions with collisionable objects and snap to the bounding box if needed
	if (place_meeting(x, y + current_vspeed, cls_Collisionable)) {
		//var _id = instance_place(x, y + current_vspeed, cls_Collisionable);
			
		while (!place_meeting(x, y + sign(current_vspeed), cls_Collisionable)) {
			y = y + sign(current_vspeed);
		}
		current_vspeed = 0;
			
	}
	/*else if (obj_Elevator.bbox_bottom == obj_Player.bbox_bottom && obj_Elevator.bbox_left <= obj_Player.bbox_left && obj_Elevator.bbox_right >= obj_Player.bbox_right) {
		current_vspeed = 0;
	}*/
	else {
		y = y + current_vspeed;
	}
	
	
}

function fnc_PlayerFSM_Mine() {
	// Gravity
	fnc_PlayerFSM_Gravity();
	
	// Check if block is near and damage/destroy if possible
	if (!still_mining && obj_Player.pickaxe_health > 0) {
		
		var _id = noone;
		var _direction = noone;
		if ((device_mouse_y(0) > obj_Player.bbox_bottom && device_mouse_y(0) < obj_Player.bbox_bottom + TILE_SIZE && device_mouse_x(0) >= obj_Player.x - obj_Player.sprite_width/2 && device_mouse_x(0) <= obj_Player.x + obj_Player.sprite_width/2)) {
			//show_debug_message("Mining down");
			_id = instance_place(x, y+1, obj_EarthBlock);
			_direction = "down";
		}		
		if ((device_mouse_x(0) > obj_Player.bbox_right && device_mouse_x(0) < obj_Player.bbox_right + TILE_SIZE && device_mouse_y(0) >= obj_Player.y - obj_Player.sprite_height/2 && device_mouse_y(0) <= obj_Player.y + obj_Player.sprite_height/2)) {
			//show_debug_message("Mining right");
			_direction = "right";
			_id = instance_place(x+obj_Player.sprite_width/2, y, obj_EarthBlock);
		}
		if ((device_mouse_x(0) < obj_Player.bbox_left && device_mouse_x(0) > obj_Player.bbox_left - TILE_SIZE && device_mouse_y(0) >= obj_Player.y - obj_Player.sprite_height/2 && device_mouse_y(0) <= obj_Player.y + obj_Player.sprite_height/2)) {
			//show_debug_message("Mining left");
			_direction = "left";
			_id = instance_place(x-obj_Player.sprite_width/2, y, obj_EarthBlock);
		}
		// Mine block above you, introduced in 1.2.0
		if ((device_mouse_y(0) < obj_Player.bbox_top && device_mouse_y(0) > obj_Player.bbox_top - TILE_SIZE && device_mouse_x(0) >= obj_Player.x - obj_Player.sprite_width/2 && device_mouse_x(0) <= obj_Player.x + obj_Player.sprite_width/2)) {
			//show_debug_message("Mining up");
			_id = instance_place(x, y-TILE_SIZE-1, obj_EarthBlock);
			_direction = "up";
		}
		
		// Update facing
		facing = _direction == "left" ? FACING.WEST : (_direction == "right" ? FACING.EAST : facing);
	
		if (_id != noone) {
			with (_id) {
				mining_direction = _direction;
				hp = hp - obj_Player.pickaxe_damage;
				event_perform(ev_other, ev_user1);
			}
			
			if (Game.option_value[? "Sounds"]) {
				audio_play_sound(snd_Pick, 1, false);
			}
			
			obj_Player.pickaxe_health--;
		}
	}
	// Horrible hack
	still_mining = !((facing == FACING.EAST && image_index == 44) || (facing == FACING.WEST && image_index == 53));
	
	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
}

function fnc_PlayerFSM_Die() {
	if (last_state_name != state_name) {
		facing = FACING.EAST;
		if (obj_Player.alarm[3] == -1) {
			obj_Player.alarm[3] = (animation_lengths[? "Die"]-1) * animation_speeds[? "Die"];		
		}
		obj_Player.sprite_index = spr_Death;
		obj_Player.image_index = 0;
		controllable = false;
	
		audio_stop_all();
		if (Game.option_value[? "Music"] && !audio_is_playing(Game.sound_death)) {				
			Game.sound_death = audio_play_sound(snd_Death, 1, false);
			//audio_sound_gain(_snd, 0.5, 0);
		}
	
		if (Game.alarm[6] == -1) {			
			// Using "rescue transition" but it's really just a transition		
			Game.rescue_transition_type = 1;	
			Game.alarm[6] = DEATH_TRANSITION_TIME;
		}
	
		cursor_sprite = spr_Cursor_Mouse;
		fnc_PlayerFSM_Transition();
	}
	
}

function fnc_PlayerFSM_GetInput() {
	
	// Detect gamepad
	var _maxpads = gamepad_get_device_count();
	Game.primary_gamepad = -1;
	
	var _i=0; 
	while (_i < _maxpads && Game.primary_gamepad == -1) {
		Game.primary_gamepad = gamepad_is_connected(_i) ? _i : -1;
		_i++;
	}
	
	// Keyboard
	var _elevator_move = obj_Elevator.player_on_elevator && obj_Elevator.is_moving;
	var _elevator_jump = obj_Elevator.player_on_elevator && (obj_Elevator.on_base || obj_Elevator.is_moving);
	//show_debug_message(_elevator);
	//show_debug_message(string(obj_Player.bbox_bottom) +" "+ string(obj_Elevator.bbox_bottom-1));
	
	player_horizontal_input = !_elevator_move * (keyboard_check(Game.controls[? "right"]) - keyboard_check(Game.controls[? "left"]));
	if (Game.primary_gamepad != -1) {
		player_horizontal_input = !_elevator_move * ((gamepad_axis_value(Game.primary_gamepad, gp_axislh) > GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "right"])) - (gamepad_axis_value(Game.primary_gamepad, gp_axislh) < -GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "left"])));
	}
	
	player_jump_input = !_elevator_jump * keyboard_check_pressed(Game.controls[? "jump"]);
	if (Game.primary_gamepad != -1) {
		player_jump_input = !_elevator_jump * (gamepad_button_check_pressed(Game.primary_gamepad, gp_face1) || keyboard_check_pressed(Game.controls[? "jump"]));
	}
	
	player_mine_input = device_mouse_check_button(0,mb_left) && 
						((device_mouse_x(0) > obj_Player.bbox_right && device_mouse_x(0) < obj_Player.bbox_right + TILE_SIZE && device_mouse_y(0) >= obj_Player.y - obj_Player.sprite_height/2 && device_mouse_y(0) <= obj_Player.y + obj_Player.sprite_height/2) ||
						(device_mouse_x(0) < obj_Player.bbox_left && device_mouse_x(0) > obj_Player.bbox_left - TILE_SIZE && device_mouse_y(0) >= obj_Player.y - obj_Player.sprite_height/2 && device_mouse_y(0) <= obj_Player.y + obj_Player.sprite_height/2) ||
						(device_mouse_y(0) > obj_Player.bbox_bottom && device_mouse_y(0) < obj_Player.bbox_bottom + TILE_SIZE && device_mouse_x(0) >= obj_Player.x - obj_Player.sprite_width/2 && device_mouse_x(0) <= obj_Player.x + obj_Player.sprite_width/2) ||
						// Mine block above you, introduced in 1.2.0
						(device_mouse_y(0) < obj_Player.bbox_top && device_mouse_y(0) > obj_Player.bbox_top - TILE_SIZE && device_mouse_x(0) >= obj_Player.x - obj_Player.sprite_width/2 && device_mouse_x(0) <= obj_Player.x + obj_Player.sprite_width/2)
						);
	
}

function fnc_PlayerFSM_Animate(_reset_animation) {
	
	animation_frame_count = _reset_animation ? 0 : ((animation_frame_count + 1) % animation_lengths[? animation_name]);
	animation_frame = animation_frame_count * animation_spacings[? animation_name];
	
	/* Currently working in 4-directions if the sprite is a multi-frame sprite (imported from spritesheet) like so:
		
		[ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
			0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23
		|----------||----------------------||----------------------------------|
			idle             move                          attack
		    ^           ^                       ^
		startindex   startindex              startindex
			
		order of directions determined by FACING enum
		sprite should have centerpoint as origin
		
	*/
		
	if (animation_dirs >= 4) {
		image_index = animation_startindices[? animation_name] + facing * animation_lengths[? animation_name] + animation_frame;		
		if  (Game.debug) {
			show_debug_message(name+" @ "+state_name+" facing "+string(facing)+": ("+string(_reset_animation)+") "+string(animation_startindices[? animation_name])+"+"+string(facing)+"*"+string(animation_lengths[? animation_name])+"+"+string(animation_frame)+"="+string(image_index));
		}
	}
	else if (animation_dirs	== 2) {		
		image_xscale = (facing == FACING.EAST ? -base_xscale : base_xscale);			
		image_index = player_horizontal_input == -1 ? 1 : (player_horizontal_input == 1 ? 2 : 0);			
	}
	else {
		image_index = animation_startindices[? animation_name] + animation_frame;
	}

}

function fnc_PlayerFSM_ResetAnimation() {
	fnc_PlayerFSM_Animate(true);
	alarm[0] = animation_speeds[? animation_name];
}

function fnc_PlayerFSM_OnGround() {
	//return place_meeting(x, y+1,cls_Collisionable);	
	//var _left = instance_position(x-sprite_width/2, bbox_bottom+1, cls_Collisionable);
	//var _right = instance_position(x+sprite_width/2, bbox_bottom+1, cls_Collisionable);
	//show_debug_message(string(_left)+" "+string(_right));
	//return ((_left != id && _left != noone) || (_right != id && _right != noone));	
	return position_meeting(x-JUMP_TOLERANCE, bbox_bottom+1, cls_Collisionable) || position_meeting(x+JUMP_TOLERANCE, bbox_bottom+1, cls_Collisionable);
}

function fnc_PlayerFSM_Transition() {
	last_state = state;
	last_state_name = state_name;
	
	// Transition matrix	
	if (state_name == "Idle") {		
		if (hp <= 0) {		
			state = fnc_PlayerFSM_Die;
			state_name = "Die";		
		}
		else if (controllable && player_mine_input) {
			state = fnc_PlayerFSM_Mine;	
			state_name = "Mine";
		}
		else if (controllable && player_horizontal_input != 0) {			
			state = fnc_PlayerFSM_Walk;	
			state_name = "Walk";
		}
		else if (controllable && player_jump_input != 0) {			
			state = fnc_PlayerFSM_Jump;	
			state_name = "Jump";
		}
		else {
			state = fnc_PlayerFSM_Idle;	
			state_name = "Idle";		
		}
	}
	else if (state_name == "Walk") {
		if (hp <= 0) {		
			state = fnc_PlayerFSM_Die;
			state_name = "Die";		
		}
		else if (controllable && player_mine_input) {
			state = fnc_PlayerFSM_Mine;	
			state_name = "Mine";
		}
		else if (controllable && player_jump_input != 0) {			
			state = fnc_PlayerFSM_Jump;	
			state_name = "Jump";
		}
		else if (controllable && player_horizontal_input == 0) {			
			state = fnc_PlayerFSM_Idle;	
			state_name = "Idle";
		}		
		else {
			state = fnc_PlayerFSM_Walk;	
			state_name = "Walk";		
		}
	}
	else if (state_name == "Jump") {
		if (hp <= 0) {		
			state = fnc_PlayerFSM_Die;
			state_name = "Die";		
		}
		else if (controllable && player_horizontal_input != 0) {			
			state = fnc_PlayerFSM_Walk;	
			state_name = "Walk";
		}
		else {
			state = fnc_PlayerFSM_Idle;	
			state_name = "Idle";		
		}
	}
	else if (state_name == "Mine") {
		if (hp <= 0) {		
			state = fnc_PlayerFSM_Die;
			state_name = "Die";		
		}
		else if (!still_mining) {
			state = fnc_PlayerFSM_Idle;	
			state_name = "Idle";
		}
		else {
			state = fnc_PlayerFSM_Mine;	
			state_name = "Mine";		
		}
	}
	else { // Die
		
	}
	
	// Reset animation
	reset_animation = (state != last_state);
	animation_name = state_name;
	
	if (reset_animation) {
		if (Game.debug) {
			//show_debug_message(name+" transitioning from "+string(last_state)+" "+last_state_name+" to "+string(state)+" "+state_name);
		}
		fnc_PlayerFSM_ResetAnimation();
	}
}