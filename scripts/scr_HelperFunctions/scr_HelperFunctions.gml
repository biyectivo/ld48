// Function to (re)initialize game variables
function fnc_InitializeGameStartVariables() {
	paused = false;
	lost = false;
	not_started = true;
	
	level = 1;
	total_score = 0;
	death_message_chosen = "";
	
	current_step = 0;	
	
	scoreboard_queried = false;
	scoreboard = [];
	scoreboard_html5 = [];
	
	http_call = "";
	http_get_id = -1;
	http_return_status = noone;
	http_return_status_insert = noone;	
	http_return_status_query = noone;
	http_get_id_update = noone;
	http_get_id_result = noone;
	http_get_id_query = noone;
	
	current_scoreboard_updates = 0;
	max_scoreboard_updates = 1;
	timer_scoreboard_updates = 180;
	
	sun = noone;
	
	// Reset alarms (except for 0, which is center screen), since Game is persistent
	for (var _i=1; _i<=11; _i++) {
		Game.alarm[_i] = -1;
	}
	
	current_depth = 0;
	max_depth = 0;
	total_gold = 0;
	
	
	draw_shop = false;
	fullscreen_change = false;
	
	shop_message = "";
	
	
	sound_death = noone;

	show_tutorial_message = true;
	
	tutorial = [];
	tutorial_index = 0;
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Welcome to Shiny Depths!  [scale,0.3](press [c_purple]RMB[c_white] to skip tutorial)");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Move and jump with [c_purple]WASD[c_white] by default (you can remap them in Options)");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Use [c_purple]LMB[c_white] to mine, or interact with the shop.");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Use [c_purple]RMB[c_white] to switch on your headlight while underground.");	
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Your headlight is critical for survival and to detect precious materials.");	
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Careful, your health will decline while underground.");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Press [c_purple]ESC[c_white] to pause the game.");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Monitor your [c_purple]health[c_white], headlight [c_purple]battery[c_white] level, [c_purple]pickaxe[c_white] sharpness and carrying capacity.");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Go to the surface shop to [c_purple]sell[c_white] your stuff and [c_purple]upgrade[c_white] your gear.");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5][c_purple]Dig carefully[c_white] so you can get back to the surface.");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]If all else fails, press the [c_purple]call for rescue[c_white] button... you will lose your gems, though!");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Now [c_purple]jump[c_white] onto that elevator and descend (with [c_purple]S[c_white] by default)!");
	array_push(tutorial, "[fnt_Details][c_white][fa_center][fa_middle][scale,0.5]Explore deeper and deeper and survive! Can you reach the center of the Earth?");
	
	fnc_InitializeShop();
	fnc_InitializeJewels();
	Game.alarm[4] = TUTORIAL_MSG_DURATION;
	Game.alarm[5] = 60;
	
	particle_type_minedust = noone;
	
	show_rescue_confirmation_dialog = false;	
	rescue_transition_type = 0;
	num_rescues = 0;
}


// Generate mine
function fnc_GenerateMine() {
	var _offset_y = 8+8*TILE_SIZE;
	var _num_cols = ceil(room_width/TILE_SIZE);
	var _num_rows = os_browser == browser_not_a_browser ? 150 : 50; //20; //ceil((room_height-_offset_y)/TILE_SIZE);
	
	var _generated_ld = false;
	
	// Generate elevator
	var _shaft = instance_create_layer(floor(_num_cols/2)*TILE_SIZE,8+7*TILE_SIZE, layer_get_id("lyr_Instances_Below"), obj_ElevatorShaft);			
	var _elevator = instance_create_layer(floor(_num_cols/2)*TILE_SIZE,8+7*TILE_SIZE, layer_get_id("lyr_Instances_Above"), obj_Elevator);			
	
	
	Game.num_generated_jewels = 0;
	
	// Blocks
	for (var _row = 0; _row < _num_rows-1; _row++) {
		for (var _col = 0; _col < _num_cols; _col++) {		
			var _rnd = random(1);
			var _x = _col*TILE_SIZE;
			var _y = _offset_y + _row*TILE_SIZE;			
			// Generate grass blocks & mine entrance
			if (_row == 0 && _col != floor(_num_cols/2)) {
				var _block = instance_create_layer(_x, _y, layer_get_id("lyr_Blocks"), obj_GrassBlock);			
			}
			// Generate earth blocks (except shaft)
			else if (_row > 4 || _row <= 4 && _col != floor(_num_cols/2) && _col != floor(_num_cols/2)-1 && _col != floor(_num_cols/2)+1 /*&& _rnd <= 0.7*/) {				
				var _block = instance_create_layer(_x, _y, layer_get_id("lyr_Blocks"), obj_EarthBlock);
				with (_block) {
					max_hp = irandom_range(2,3) + floor(_row / 5);
					if (random(1) < UNBROKEN_BLOCK_PROBABILITY) {
						hp = max_hp;
					}
					else {
						hp = irandom_range(1, max_hp-1);
					}
					block_depth = _row;	
				}
				
				// Generate jewels - changed in 1.2.0 to better distribution per row
				if (_rnd < JEWEL_GENERATION_PROBABILITY && _col > 0 && _col < _num_cols-1) {					
					Game.num_generated_jewels++;
					var _generables = fnc_CheckWhichJewels(_row);
					//var _probs = fnc_CheckWhichJewelsProbabilities(_row);
					//show_debug_message("Row: "+string(_row)+" Jewels: "+string(_generables)+ "Probs: "+string(_probs));
					
					if (array_length(_generables) > 0) {
						var _jewel = instance_create_layer(_x, _y, layer_get_id("lyr_Jewels"), obj_Jewel);					
						var _idx = fnc_ChooseProb(_generables, []);
						//show_debug_message("Generated "+string(_idx)+" "+Game.jewels[_idx].name+" at "+string(_row)+","+string(_col));
						with (_jewel) {
							image_index = _idx;
						}
					}
					else {
						//show_debug_message("Empty at "+string(_row)+","+string(_col));
					}
				}
			}
			// Generate elevator shaft
			else if (_row < 4 && _col == floor(_num_cols/2)) {
				var _shaft = instance_create_layer(_x, _y, layer_get_id("lyr_Instances_Below"), obj_ElevatorShaft);				
			}
			// Generate elevator base
			else if (_row == 4 && _col == floor(_num_cols/2)) {
				var _base = instance_create_layer(_x, _y, layer_get_id("lyr_Instances_Below"), obj_ElevatorBase);				
			}
			
		}
	}

	// Generate LD gem
	
	// blocks
	for (var _col = 0; _col < _num_cols; _col++) {
		_row = _num_rows-1;
		var _x = _col*TILE_SIZE;
		var _y = _offset_y + _row*TILE_SIZE;	
		var _block = instance_create_layer(_x, _y, layer_get_id("lyr_Blocks"), obj_EarthBlock);
		with (_block) {
			max_hp = irandom_range(2,3) + floor(_row / 5);
			if (random(1) < UNBROKEN_BLOCK_PROBABILITY) {
				hp = max_hp;
			}
			else {
				hp = irandom_range(1, max_hp-1);
			}
			block_depth = _row;	
		}
	}
	// gem
	
	var _row = _num_rows-1;
	var _col = irandom_range(1, _num_cols-2);
	var _x = _col*TILE_SIZE;
	var _y = _offset_y + _row*TILE_SIZE;			
	var _jewel = instance_create_layer(_x, _y, layer_get_id("lyr_Jewels"), obj_Jewel);					
	var _idx = array_length(jewels)-1;
	
	with (_jewel) {
		image_index = _idx;
	}
	Game.num_generated_jewels++;
	
	// End of world
	for (var _col = 0; _col < _num_cols; _col++) {
		_row = _num_rows;
		var _x = _col*TILE_SIZE;
		var _y = _offset_y + _row*TILE_SIZE;			
		var _block = instance_create_layer(_x, _y, layer_get_id("lyr_Blocks"), obj_EndOfWorld);
		with (_block) {
			image_index = irandom_range(0,sprite_get_number(spr_EndOfWorld)/2-1);	
		}
		
		_row = _num_rows+1;
		var _x = _col*TILE_SIZE;
		var _y = _offset_y + _row*TILE_SIZE;			
		var _block = instance_create_layer(_x, _y, layer_get_id("lyr_Blocks"), obj_EndOfWorld);
		with (_block) {
			image_index = irandom_range(sprite_get_number(spr_EndOfWorld)/2, sprite_get_number(spr_EndOfWorld)-1);	
		}
	}
	
	
	
	// Sun
	
	
	sun = instance_create_layer(20*TILE_SIZE, 4*TILE_SIZE, layer_get_id("lyr_Light"), obj_StaticLight);
	with (sun) {
		light.blend = $ffeeee;
		light.xscale = 9;
		light.yscale = 9;
		light.penumbraSize = 32;
		light.sprite = sLight128;
	}
	
	
	
}


function fnc_CheckWhichJewels(_level) {
	var _n = array_length(Game.jewels);
	var _generables = [];
	var _probs = [];
	var _prob = 0;
	// exclude last jewel (Ludumdareite)
	for (var _i=0; _i<_n-1; _i++) {
		if (Game.jewels[_i].min_level <= _level) {			
			array_push(_generables, _i);			
			_prob = _prob + Game.jewels[_i].probability;
		}
	}
	// exclude last jewel (Ludumdareite)
	var _n = array_length(_generables);	
	for (var _i=0; _i<_n-1; _i++) {
		var _idx = _generables[_i];
		var _individual_prob = Game.jewels[_idx].probability;		
		array_push(_probs, _individual_prob/_prob);
	}
	
	return _generables;
}

function fnc_CheckWhichJewelsProbabilities(_level) {
	var _n = array_length(Game.jewels);
	var _generables = [];
	var _probs = [];
	var _prob = 0;
	// exclude last jewel (Ludumdareite)
	for (var _i=0; _i<_n-1; _i++) {
		if (Game.jewels[_i].min_level <= _level) {			
			array_push(_generables, _i);			
			_prob = _prob + Game.jewels[_i].probability;
		}
	}
	
	var _n = array_length(_generables);	
	// exclude last jewel (Ludumdareite)
	for (var _i=0; _i<_n-1; _i++) {
		var _idx = _generables[_i];
		var _individual_prob = Game.jewels[_idx].probability;		
		array_push(_probs, _individual_prob/_prob);
	}
	
	return _probs;
}

// Generate ambient
function fnc_DrawAmbient() {
	fnc_BackupDrawParams();
	// Draw Sky
	draw_rectangle_color(0, 0, room_width, 16+8*TILE_SIZE, $ffabab, $ffc7c7, $ffe7e7, $fff7f7, false); 
	
	fnc_RestoreDrawParams();
}
	






// Update scoreboard
function fnc_UpdateScoreboard() {
	if (ENABLE_SCOREBOARD && Game.lost && Game.total_score > 0) {		
		var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/score_insert.php";
		var _hash = sha1_string_utf8(Game.option_value[? Game.option_items[5]]+Game.scoreboard_game_id+string(Game.total_score)+SCOREBOARD_SALT);
		var _params = "user="+Game.option_value[? Game.option_items[5]]+"&game="+Game.scoreboard_game_id+"&score="+string(Game.total_score)+"&h="+_hash;
		
		http_get_id_update = http_get(_scoreboard_url + "?" + _params);		
	}
	else {
		http_get_id_update = -1;
	}
}


/// @function fnc_ChooseProb(choose_array, array_probs)
/// @description Chooses a random value from the array with specified probability distribution
/// @param choose_array The array of values to choose from. If empty array, the function will return the index instead.
/// @param array_probs The probability array of each values from the list. If empty, use uniform distribution.
/// @return The chosen element

function fnc_ChooseProb(_choose_array, _array_probs) {
	var _n_choose = array_length(_choose_array);
	var _n_probs = array_length(_array_probs);
	
	if (_n_probs == 0 && _n_choose == 0) {  // Error
		throw("Error on fnc_ChooseProb, empty arrays provided.");
	}
	else {
		if (_n_probs == 0) {	 // Set uniform distribution
			var _probs = array_create(_n_choose);
			var _k = 0;
			for (var _i=0; _i<_n_choose; _i++) {
				if (_i < _n_choose - 1) {
					_probs[_i] = 1/_n_choose;
					_k=_k+_probs[_i];
				}
				else {
					_probs[_i] = 1-_k;				
				}
			}
		}
		else {	// Use what has been given
			_probs = _array_probs;			
		}
	
		var _rnd = random(1);
		var _i = 0;
		var	_currProb = _probs[_i];
		var _cumProb = _currProb;
		while (_rnd > _cumProb) {
			_i++;
			var	_currProb = _probs[_i];
			var _cumProb = _cumProb + _currProb;
		}
		if (_n_choose == 0) {
			return _i;
		}
		else {		
			return _choose_array[_i];
		}
	}
}

/// @function fnc_ChooseProbList(choose_list, array_probs)
/// @description Chooses a random value from the list with specified probability distribution
/// @param choose_list The list of values to choose from
/// @param array_probs The probability array of each values from the list
/// @return The chosen element

function fnc_ChooseProbList(_choose_list, _array_probs) {
	var _rnd = random(1);
	var _i = 0;
	var	_currProb = _array_probs[_i];
	var _cumProb = _currProb;
	while (_rnd > _cumProb) {
		_i++;
		var	_currProb = _array_probs[_i];
		var _cumProb = _cumProb + _currProb;
	}
	return _choose_list[| _i];
}



/// @function string_to_list(string, delimiter)
/// @arg string The string to parse
/// @arg delimiter The delimiter to use
/// @return A DS List with the contents of the string separated by the chosen delimiter.
function string_to_list() {

	var _string = argument[0];
	var _delimiter = argument[1];

	var _result = ds_list_create();

	var _n = string_length(_string);

	if (_n > 0) {
		var _temp = _string;
	
		var _fin = string_pos(_delimiter, _temp);
		while (_fin > 0) {
			ds_list_add(_result, string_copy(_temp, 1, _fin-1));
			_temp = string_copy(_temp, _fin + 1, _n);
			_fin = string_pos(_delimiter, _temp);
		}
		ds_list_add(_result, string_copy(_temp, 1, string_length(_temp)));
	}

	return _result;
}


/// @function fnc_KeyToString(_key)
/// @arg _key The keycode to name
/// @return The reeadable name of the keycode

function fnc_KeyToString(_key) {
	if (_key >= 48 && _key <= 90) { 
		return chr(_key);
	}
	else {
		switch(_key) {
		    case -1: return "Unassigned";
		    case vk_backspace: return "Backspace";
		    case vk_tab: return "Tab";
		    case vk_enter: return "Enter";
		    case vk_lshift: return "Left Shift";
		    case vk_lcontrol: return "Left Ctrl";
		    case vk_lalt: return "Left Alt";
			case vk_rshift: return "Right Shift";
		    case vk_rcontrol: return "Right Ctrl";
		    case vk_ralt: return "Right Alt";
			case vk_shift: return "Shift";
		    case vk_control: return "Ctrl";
		    case vk_alt: return "Alt";
			case vk_printscreen: return "Print Screen";
		    case vk_pause: return "Pause/Break";
		    case 20: return "Caps Lock";
		    case vk_escape: return "Esc";
			case vk_space: return "Space";
		    case vk_pageup: return "Page Up";
		    case vk_pagedown: return "Page Down";
		    case vk_end: return "End";
		    case vk_home: return "Home";
		    case vk_left: return "Left Arrow";
		    case vk_up: return "Up Arrow";
		    case vk_right: return "Right Arrow";
		    case vk_down: return "Down Arrow";
		    case vk_insert: return "Insert";
		    case vk_delete: return "Delete";
			case vk_divide: return "/";
		    case vk_numpad0: return "Numpad 0";
		    case vk_numpad1: return "Numpad 1";
		    case vk_numpad2: return "Numpad 2";
		    case vk_numpad3: return "Numpad 3";
		    case vk_numpad4: return "Numpad 4";
		    case vk_numpad5: return "Numpad 5";
		    case vk_numpad6: return "Numpad 6";
		    case vk_numpad7: return "Numpad 7";
		    case vk_numpad8: return "Numpad 8";
		    case vk_numpad9: return "Numpad 9";
		    case vk_multiply: return "Numpad *";
		    case vk_add: return "Numpad +";
			case vk_decimal: return "Numpad .";
		    case vk_subtract: return "Numpad -";    
		    case vk_f1: return "F1";
		    case vk_f2: return "F2";
		    case vk_f3: return "F3";
		    case vk_f4: return "F4";
		    case vk_f5: return "F5";
		    case vk_f6: return "F6";
		    case vk_f7: return "F7";
		    case vk_f8: return "F8";
		    case vk_f9: return "F9";
		    case vk_f10: return "F10";
		    case vk_f11: return "F11";
		    case vk_f12: return "F12";
		    case 144: return "Num Lock";
		    case 145: return "Scroll Lock";
		    case ord(";"): return ";";
		    case ord("="): return "=";
		    case ord("\\"): return "\\";    
		    case ord("["): return "[";
		    case ord("]"): return "]";
			default: return "other";
		}
	}
}

/// @function fnc_BackupDrawParams
/// @description Backup the draw parameters to variables

function fnc_BackupDrawParams() {
	tmpDrawFont = draw_get_font();
	tmpDrawHAlign = draw_get_halign();
	tmpDrawVAlign = draw_get_valign();
	tmpDrawColor = draw_get_color();
	tmpDrawAlpha = draw_get_alpha();	
}

/// @function fnc_RestoreDrawParams
/// @description Restore the draw parameters from variables

function fnc_RestoreDrawParams() {
	draw_set_font(tmpDrawFont);
	draw_set_halign(tmpDrawHAlign);
	draw_set_valign(tmpDrawVAlign);
	draw_set_color(tmpDrawColor);
	draw_set_alpha(tmpDrawAlpha);	
}

function base_convert(_string_number, _old_base, _new_base) {
	var number, oldbase, newbase, out;
    number = string_upper(_string_number);
    oldbase = _old_base;
    newbase = _new_base;
    out = "";
 
    var len, tab;
    len = string_length(number);
    tab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
    var i, num;
    for (i=0; i<len; i+=1) {
        num[i] = string_pos(string_char_at(number, i+1), tab) - 1;
    }
 
    do {
        var divide, newlen;
        divide = 0;
        newlen = 0;
        for (i=0; i<len; i+=1) {
            divide = divide * oldbase + num[i];
            if (divide >= newbase) {
                num[newlen] = divide div newbase;
                newlen += 1;
                divide = divide mod newbase;
            } else if (newlen  > 0) {
                num[newlen] = 0;
                newlen += 1;
            }
        }
        len = newlen;
        out = string_char_at(tab, divide+1) + out;
    } until (len == 0);
 
    return out;
}


function in(_element, _array) {
	if (array_length(_array) == 0) {
		return false;
	}
	else {
		var _i=0;
		var _n = array_length(_array);
		var _found = false;
		while (_i<_n && !_found) {
			if (_array[_i] == _element) {
				_found = true;
			}
			else {
				_i++;
			}
		}
	}
	return _found;
}

#region String functions

/// @function		fnc_StringToList()
/// @description	Takes a string with separators and returns a DS list of the separated string
/// @param			string - Source string to split
/// @param			separator - Optional string separator to use. Default: comma
/// @param			remove leading and trailing spaces - Optional boolean to remove leading and trailing spaces on each item of the list. Default: false
/// @return			A DS list with the string split into the parts
function fnc_StringToList() {
	if (argument_count == 0) {		
		throw ("String argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _string = argument[0];
	}
	if (argument_count >= 2) {		
		var _separator = argument[1];
		if (string_length(_separator) != 1) {
			_separator = ",";
		}
	}
	else {
		var _separator = ",";
	}
	if (argument_count >= 3) {
		var _remove_lead_trail_spaces = argument[2];
	}
	else {
		var _remove_lead_trail_spaces = false;
	}
	
		
	// Process and split
	var _list = ds_list_create();	
	var _substring = _string;
	var _next_separator = string_pos(_separator, _substring);
	while (_next_separator != 0) {
		var _found = string_copy(_substring, 0, _next_separator-1);
		if (_remove_lead_trail_spaces) {			
			_found = fnc_String_lrtrim(_found);
		}
		
		if (string_length(_found) > 0) {
			ds_list_add(_list, _found);		
		}
		
		_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
		var _next_separator = string_pos(_separator, _substring);
	}
	ds_list_add(_list,_substring);
	
	return _list;
}


function fnc_String_cleanse() {
	var _str = argument[0];
	for (var _j=0; _j<32; _j++) {
		_str = string_replace_all(_str, chr(_j), "");
	}
	return _str;
}

function fnc_String_lrtrim() {
	var _str = argument[0];
	var _j=0;
	var _l=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j<_l) {
		_j++;
	}
	_str = string_copy(_str, _j, _l);
			
	var _j=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j>=0) {
		_j--;
	}
	_str = string_copy(_str, 0, _j);
	return _str;
}

#endregion

function fnc_FormatString(_string_number) {
	var _n = string_length(_string_number);
	var _new_text = "";
	var _digits = 0;
	for (var _i=_n-1; _i>=0; _i--) {
		_digits++;
		_new_text = string_copy(_string_number, _i+1, 1) + _new_text;
		if (_digits % 3 == 0 && _i > 0) {
			_new_text = "," + _new_text;
		}	
	}
	return _new_text;
}



function fnc_InitializeShop() {
	
	pickaxe_level_durability = [];
	array_push(pickaxe_level_durability, 100);
	array_push(pickaxe_level_durability, 150);
	array_push(pickaxe_level_durability, 200);
	array_push(pickaxe_level_durability, 300);
	
	pickaxe_level_strength = [];
	array_push(pickaxe_level_strength, 1);
	array_push(pickaxe_level_strength, 2);
	array_push(pickaxe_level_strength, 3);
	array_push(pickaxe_level_strength, 5);
	
	headlight_level_strength = [];
	array_push(headlight_level_strength, 6);
	array_push(headlight_level_strength, 9);
	array_push(headlight_level_strength, 14);
	
	headlight_level_color = [];
	array_push(headlight_level_color, c_yellow);
	array_push(headlight_level_color, c_yellow);
	array_push(headlight_level_color, $f8ff74);
	
	bag_level_capacity = [];
	array_push(bag_level_capacity, 50);
	array_push(bag_level_capacity, 80);
	array_push(bag_level_capacity, 150);
	
	// Shop
	shop = [];
	
	shop_item = {
		name : "Replace headlight battery",
		description : "Get a fresh set of batteries to work with.",
		currently_available : function() {
			return (instance_exists(obj_Player) && obj_Player.helmet_light_battery < 100);
		},
		price : 1000,
		unlocked : true,
		bought : false,
		shop_availability : -1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Repair pickaxe",
		description : "Sharpen your pickaxe to keep mining deeper.",
		currently_available : function() {
			return (instance_exists(obj_Player) && obj_Player.pickaxe_health < 100);
		},
		price : 1500,
		unlocked : true,
		bought : false,
		shop_availability : -1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Band-aid",
		description : "Cure your light wounds.",
		currently_available : function() {
			return (instance_exists(obj_Player) && obj_Player.hp < obj_Player.max_hp);
		},
		price : 1500,
		unlocked : true,
		bought : false,
		shop_availability : -1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "First-aid kit",
		description : "Replenish your health completely.",
		currently_available : function() {
			return (instance_exists(obj_Player) && obj_Player.hp < obj_Player.max_hp);
		},
		price : 5000,
		unlocked : true,
		bought : false,
		shop_availability : -1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade pickaxe - Level 2",
		description : "Increase pickaxe durability by "+string(round(100*pickaxe_level_durability[1]/pickaxe_level_durability[0])-100)+"% and strength by "+string(round(100*pickaxe_level_strength[1]/pickaxe_level_strength[0])-100)+"%.",
		currently_available : function() {
			return obj_Player.pickaxe_max_health == Game.pickaxe_level_durability[0];
		},
		price : 20000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade pickaxe - Level 3",
		description : "Increase pickaxe durability by "+string(round(100*pickaxe_level_durability[2]/pickaxe_level_durability[1])-100)+"% and strength by "+string(round(100*pickaxe_level_strength[2]/pickaxe_level_strength[1])-100)+"%.",
		currently_available : function() {
			return obj_Player.pickaxe_max_health == Game.pickaxe_level_durability[1];
		},
		price : 50000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade pickaxe - Level 4",
		description : "Increase pickaxe durability by "+string(round(100*pickaxe_level_durability[3]/pickaxe_level_durability[2])-100)+"% and strength by "+string(round(100*pickaxe_level_strength[3]/pickaxe_level_strength[2])-100)+"%.",
		currently_available : function() {
			return obj_Player.pickaxe_max_health == Game.pickaxe_level_durability[2];
		},
		price : 100000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade to medium satchel",
		description : "Increase carrying capacity to "+string(bag_level_capacity[1])+".",
		currently_available : function() {
			return obj_Player.bag_max_weight == Game.bag_level_capacity[0];
		},
		price : 30000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade to big backpack",
		description : "Increase carrying capacity to "+string(bag_level_capacity[2])+".",
		currently_available : function() {
			return obj_Player.bag_max_weight == Game.bag_level_capacity[1];
		},
		price : 100000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade headlight to level 2",
		description : "Increase headlight strength by "+string(round(100*headlight_level_strength[1]/headlight_level_strength[0])-100)+"%.",
		currently_available : function() {
			return obj_Player.current_light_size == Game.headlight_level_strength[0];
		},
		price : 50000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);
	
	shop_item = {
		name : "Upgrade headlight to level 3",
		description : "Increase headlight strength by "+string(round(100*headlight_level_strength[2]/headlight_level_strength[1])-100)+"% and provide cold light.",
		currently_available : function() {
			return obj_Player.current_light_size == Game.headlight_level_strength[1];
		},
		price : 100000,
		unlocked : true,
		bought : false,
		shop_availability : 1
	}	
	array_push(shop, shop_item);	
}

function fnc_InitializeJewels() {
	jewels = [];
	jewel = { index : 0, type : "material", name : "Coal", description : "Your run-of-the-mill coal lump.", value : 200, weight : 3, probability : 0.15, min_level : 3, currently_have : 0, total_had : 0, increase_block_strength : 1}; array_push(jewels, jewel);
	jewel = { index : 1, type : "material", name : "Iron", description : "An iron ore, rich in magnetite.", value : 500, weight : 10, probability : 0.12, min_level : 3, currently_have : 0, total_had : 0, increase_block_strength : 1}; array_push(jewels, jewel);
	jewel = { index : 2, type : "jewel", name : "Quartz", description : "The crystalline mineral composed of silicon and oxygen.", value : 800, weight : 5, probability : 0.1, min_level : 3, currently_have : 0, total_had : 0, increase_block_strength : 1}; array_push(jewels, jewel);
	jewel = { index : 3, type : "jewel", name : "Aquamarine", description : "A beautiful pale blue beryl.", value : 1200, weight : 5, probability : 0.08, min_level : 4, currently_have : 0, total_had : 0, increase_block_strength : 2}; array_push(jewels, jewel);
	jewel = { index : 4, type : "material", name : "Copper", description : "A copper ore, great for electrical conductivity.", value : 1500, weight : 12, probability : 0.07, min_level : 5, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 5, type : "material", name : "Tin", description : "Useful for making bronze.", value : 2000, weight : 10, probability : 0.06, min_level : 5, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 6, type : "jewel", name : "Amethyst", description : "A beautiful purple jewel.", value : 2500, weight : 5, probability : 0.05, min_level : 5, currently_have : 0, total_had : 0, increase_block_strength : 2}; array_push(jewels, jewel);
	jewel = { index : 7, type : "jewel", name : "Onyx", description : "A banded mineral with beautiful black color.", value : 2800, weight : 10, probability : 0.04, min_level : 5, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 8, type : "jewel", name : "Garnet", description : "A nice deep reddish gem.", value : 3000, weight : 5, probability : 0.04, min_level : 5, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 9, type : "jewel", name : "Turquoise", description : "A semi-precious gem with a soft texture and beautiful color.", value : 3200, weight : 8, probability : 0.03, min_level : 15, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 10, type : "material", name : "Palladium", description : "A lustrous silvery-white metal.", value : 4000, weight : 10, probability : 0.03, min_level : 5, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 11, type : "jewel", name : "Lapis Lazuli", description : "A stunning blue gem.", value : 5000, weight : 20, probability : 0.03, min_level : 10, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 12, type : "material", name : "Silver", description : "A soft, white, lustrous metal.", value : 6000, weight : 12, probability : 0.03, min_level : 10, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 13, type : "jewel", name : "Sapphire", description : "A precious blue gem.", value : 7000, weight : 10, probability : 0.03, min_level : 15, currently_have : 0, total_had : 0, increase_block_strength : 2}; array_push(jewels, jewel);
	jewel = { index : 14, type : "material", name : "Gold", description : "A precious, valuable, shiny metal.", value : 8000, weight : 15, probability : 0.02, min_level : 10, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 15, type : "material", name : "Platinum", description : "A remarkably durable metal.", value : 9000, weight : 10, probability : 0.02, min_level : 10, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 16, type : "material", name : "Iridium", description : "A precious metal coming from extraterretrial asteroids.", value : 10000, weight : 20, probability : 0.02, min_level : 15, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 17, type : "jewel", name : "Blue Diamond", description : "A beautiful, shiny blue diamond.", value : 20000, weight : 5, probability : 0.02, min_level : 20, currently_have : 0, total_had : 0, increase_block_strength : 3}; array_push(jewels, jewel);
	jewel = { index : 18, type : "material", name : "Jadeite", description : "A very nice, valuable green mineral.", value : 25000, weight : 10, probability : 0.01, min_level : 20, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 19, type : "jewel", name : "Pink Diamond", description : "An astonishing pink diamond.", value : 30000, weight : 5, probability : 0.01, min_level : 25, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 20, type : "jewel", name : "Ruby", description : "A captivating red color jewel.", value : 35000, weight : 15, probability : 0.01, min_level : 25, currently_have : 0, total_had : 0, increase_block_strength : 5}; array_push(jewels, jewel);
	jewel = { index : 21, type : "jewel", name : "Emerald", description : "A gorgeous light green gem.", value : 40000, weight : 8, probability : 0.01, min_level : 30, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 22, type : "jewel", name : "Alexandrite", description : "A beautiful gem that shows iridiscence.", value : 50000, weight : 10, probability : 0.004, min_level : 40, currently_have : 0, total_had : 0, increase_block_strength : 5}; array_push(jewels, jewel);
	jewel = { index : 23, type : "material", name : "Rhodium", description : "An extraordinarily rare metal.", value : 55000, weight : 30, probability : 0.004, min_level : 25, currently_have : 0, total_had : 0, increase_block_strength : 4}; array_push(jewels, jewel);
	jewel = { index : 24, type : "jewel", name : "Fire Opal", description : "An amazing gem with orangey-red shades.", value : 60000, weight : 15, probability : 0.004, min_level : 50, currently_have : 0, total_had : 0, increase_block_strength : 5}; array_push(jewels, jewel);
	jewel = { index : 25, type : "jewel", name : "Diamond", description : "One of the most valued gems of all.", value : 70000, weight : 5, probability : 0.004, min_level : 60, currently_have : 0, total_had : 0, increase_block_strength : 5}; array_push(jewels, jewel);
	jewel = { index : 26, type : "jewel", name : "Black Opal", description : "A precious black gem.", value : 80000, weight : 20, probability : 0.002, min_level : 70, currently_have : 0, total_had : 0, increase_block_strength : 5}; array_push(jewels, jewel);
	jewel = { index : 27, type : "jewel", name : "Tanzanite", description : "A jaw-dropping purple gem.", value : 100000, weight : 20, probability : 0.002, min_level : 80, currently_have : 0, total_had : 0, increase_block_strength : 5}; array_push(jewels, jewel);
	jewel = { index : 28, type : "special", name : "Ludumdareite", description : "A once-in-a-lifetime gem from out of this world! Happy LD48!", value : 1000000, weight : 150, probability : 0, min_level : 130, currently_have : 0, total_had : 0, increase_block_strength : 20}; array_push(jewels, jewel);

}