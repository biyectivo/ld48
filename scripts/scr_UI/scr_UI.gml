
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		
		var _title_color = "[c_white]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _w = window_get_width();
		var _h = window_get_height();
		
		var _y_title = 100;
		
		type_formatted(_w/2, _y_title, "[fnt_Headlines][fa_middle][fa_center]"+_title_color+game_title);
		
		var _n = array_length(menu_items);
		var _startY = _y_title + 100;
		var _spacing = 65;
		for (var _i = 0; _i<_n; _i++) {	
			fnc_Link(_w/2, _startY + _i*_spacing, "[fnt_Details][fa_middle][fa_center][scale, 0.5]"+_link_color+menu_items[_i], "[fnt_Details][fa_middle][fa_center][scale, 0.5]"+_link_hover_color+menu_items[_i], fnc_ExecuteMenu, _i);
		}
		
		type_formatted(_w/2, _h-100, "[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]A game by José Bonilla for [spr_LudumDare] 48");
		type_formatted(_w/2, _h-50, "[fnt_Details][fa_middle][fa_center][scale,0.3][c_gray]version "+string(VERSION));
		if (!(os_browser == browser_not_a_browser)) {
			type_formatted(_w/2, _h-10, "[fnt_Details][fa_middle][fa_center][scale,0.3][c_red]NOTE: HTML5 build performance is much worse. I limited total depth to 20 levels instead of 150. Play the Windows version for the full experience!");
		}
	}


	function fnc_Menu_0 () {		
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		room_goto(room_UI_HowToPlay);
	}


	function fnc_Menu_2 () {
		room_goto(room_UI_Options);
	}

	
	function fnc_Menu_3 () {
		room_goto(room_UI_Credits);
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = window_get_width();
		var _h = window_get_height();

		var _title_color = "[c_white]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
	
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 100;
		type_formatted( _w/2, _y_title, "[fa_middle][fa_center][fnt_Headlines]"+_title_color+"Options");
		var _startY = _y_title+100;
		var _spacing = 65;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);	
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_color+option_items[_i], "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_color+option_items[_i], "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp_struct = type_formatted(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_color+option_items[_i], false);
				
				var _slider_start = _w/2-100 + _temp_struct.bbox_width + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					type_formatted(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30, "[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%");
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_hover_color+option_items[_i]+": "+option_value[? option_items[_i]], "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_hover_color+option_items[_i]+": "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_color+option_items[_i]+": "+option_value[? option_items[_i]], "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_hover_color+option_items[_i]+": "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_color+option_items[_i], "[fa_middle][fa_left][fnt_Details][scale,0.7]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		//fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Main Menu", _link_hover_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]Return to Main Menu", fnc_ReturnToMainMenu, 0);
		fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Main Menu", _link_hover_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]Return to Main Menu", fnc_ReturnToMainMenu, 0);
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = window_get_width();
		var _h = window_get_height();
		
		var _title_color = "[c_white]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _y_title = 100;
		type_formatted(_w/2, _y_title, "[fa_middle][fa_center][fnt_Headlines][c_white]"+_title_color+"Controls");
		
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				type_formatted(_w/2, _h-90, _link_color+"[fa_middle][fa_center][fnt_Details][scale,0.8]PRESS ANY KEY TO REMAP");
				fnc_Link(_w/2, _y_title+60+_i*70, "[fa_middle][fa_center][fnt_Details][scale,0.6]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_center][fnt_Details][scale,0.6][c_purple]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2, _y_title+60+_i*70, "[fa_middle][fa_center][fnt_Details][scale,0.6]"+_link_color+control_names[? control_indices[_i]]+": [c_purple]"+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_center][fnt_Details][scale,0.6]"+_link_hover_color+control_names[? control_indices[_i]]+": [c_purple]"+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		type_formatted(_w/2, _y_title+60+_n*70, "[fa_middle][fa_center][fnt_Details][c_white]Mine/Interact: [c_purple]left mouse button");
		type_formatted(_w/2, _y_title+60+(_n+1)*70, "[fa_middle][fa_center][fnt_Details][c_white]Light/Exit shop: [c_purple]right mouse button");
		
		
		//fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
		fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Options", _link_hover_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_white]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 100;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Headlines][fa_middle][fa_center][c_white]Credits");
	var _startY = _y_title+120;
	var _spacing = 65;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	//fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Main Menu", _link_hover_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_white]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 100;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Headlines][fa_middle][fa_center][c_white]Help");
	var _startY = _y_title+100;
	var _spacing = 50;
		
	// Draw text...
	type_formatted(_w/2, _startY+0*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]You are [scale,2][spr_Player][scale,0.5], an expert miner!");	
	type_formatted(_w/2, _startY+1*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]You have decided to explore the Dareum mine nearby.");
	type_formatted(_w/2, _startY+2*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]Mining is dangerous, so you lose health while underground.");
	type_formatted(_w/2, _startY+3*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]The deeper you go, the more precious materials you can mine!");
	type_formatted(_w/2, _startY+4*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]Make sure you dig intelligently, so you can return to the surface!");
	type_formatted(_w/2, _startY+5*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]Once on the surface, sell your materials for gold!");
	type_formatted(_w/2, _startY+6*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle]Also recover health, upgrade and repair your gear at the shop.");
	type_formatted(_w/2, _startY+7*_spacing, _main_text_color+"[fnt_Details][scale,0.5][fa_center][fa_middle][c_purple]Prove you can get to the center of the Earth - just keep digging DEEPER & DEEPER!");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Main Menu", _link_hover_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]Return to Main Menu", fnc_ReturnToMainMenu, 0);
	//fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_white]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Headlines]Game Paused");
	type_formatted(_w/2, 160, _main_text_color+"[fa_center][fnt_Details][scale,0.7]Press ESC to resume");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Main Menu", _link_hover_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][c_white]Return to Main Menu", fnc_ReturnToMainMenu, 0);
	//fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_white]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	draw_set_alpha(0.9);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	draw_set_alpha(1);
	
	type_formatted(_w/2, -20, _title_color+"[fa_top][fa_center][fnt_Headlines][scale,2][c_white]+");
	if (death_message_chosen == "") {
		death_message_chosen = choose("Requiescat in Pace", "You died", "Goodbye cruel world", "You bit the dust", "You met your maker", "You passed away");
	}
	type_formatted(_w/2, 200, _main_text_color+"[fa_middle][fa_center][fnt_Headlines][scale,0.5][c_white]"+death_message_chosen);
	type_formatted(_w/2-600, 280, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_white]Max depth reached: [c_gray]"+string(Game.max_depth));
	type_formatted(_w/2-600, 320, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_white]Total gems mined: [c_gray]"+string(fnc_HistoricGems())+" / "+string(Game.num_generated_jewels));
	type_formatted(_w/2-600, 360, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_white]Total value mined: [c_gray]$ "+fnc_FormatString(string(fnc_HistoricValue())));
	type_formatted(_w/2-600, 400, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_white]Total rescues: [c_gray]"+fnc_FormatString(string(Game.num_rescues)));
	if (Game.max_depth >= 149) {
		type_formatted(_w/2-600, 440, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_random]You reached the center of the Earth!");
	}
	if (Game.jewels[array_length(Game.jewels)-1].total_had == 1) {
		type_formatted(_w/2-600, 500, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_random][wrap,900]You recovered the mysterious Ludumdareite!!!");
	}
	
	var _n = array_length(Game.jewels);
	var _jewel_txt;
	var _k = 0;
	type_formatted(_w/2-200, 280, _link_hover_color+"[fa_middle][fa_left][fnt_Details][scale,0.4][c_white]Gems mined:");
	var _column = 1;
	for (var _i=0; _i<_n-1; _i++) {
		if (Game.jewels[_i].total_had > 0) {			
			_k++;
			if (_k % 7 == 0) {
				_column++;
				_k=1;
			}
			_jewel_txt = "[fa_left][fa_middle][c_white][fnt_Details][spr_Jewels,"+string(Game.jewels[_i].index)+"][scale,0.3] "+Game.jewels[_i].name+" [c_gray]x"+string(Game.jewels[_i].total_had);
			type_formatted(_w/2-200+200*(_column-1), 280+_k*40, _jewel_txt);
		}
	}
		
	
	if (ENABLE_SCOREBOARD) {
		// Scoreboard
		if (!scoreboard_queried) {
			//http_call = "query_scoreboard";
			var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/scoreboard.php?game="+scoreboard_game_id+"&limit=5";
			http_get_id_query = http_get(_scoreboard_url);
			scoreboard_queried = true;
			if (current_scoreboard_updates < max_scoreboard_updates) {				
				Game.alarm[3] = timer_scoreboard_updates;
				current_scoreboard_updates++;
			}
		}
		else {
			if (http_return_status_query == 200) {
				
							
				var _list = ds_map_find_value(scoreboard_html5, "default");
				var _n = ds_list_size(_list);
				for (var _i=0; _i<_n; _i++) {
					var _map = ds_list_find_value(_list, _i);
					//show_debug_message(string(_i)+": "+string(_map[? "username"])+" = "+string(_map[? "game_score"]));
					draw_set_color(c_white);
						
					if (_i==0) {
						var _total_high_score = _map[? "game_score"];
					}
					else if (_i==4) {
						var _number_5_score	= _map[? "game_score"]; 
					}
					
					type_formatted(80, 400+_i*50, "[fa_right][scale,0.6]#"+string(_i+1));
					type_formatted(120, 400+_i*50, "[fa_left][scale,0.6]"+_map[? "username"]);
					type_formatted(500, 400+_i*50, "[fa_right][scale,0.6]"+_map[? "game_score"]);
				}
				
				
				/*particle_type_highscore =	part_type_create();		
				part_type_scale(particle_type_highscore, 1, 1);
				part_type_size(particle_type_highscore, 0.25, 0.35, 0, 0);
				part_type_life(particle_type_highscore, 5, 15);			
				part_type_shape(particle_type_highscore, pt_shape_star);
				
				part_type_alpha2(particle_type_highscore, 0.8, 0.0);
				part_type_speed(particle_type_highscore, 2, 5, 0, 0);
				part_type_direction(particle_type_highscore, 0, 360, 0, 20);
				part_type_orientation(particle_type_highscore, -10, 10, 0, 0, false);
				part_type_blend(particle_type_highscore, true);
		
		
				// High score indication
				if (Game.total_score >= _total_high_score) {
					var _txt = "YOU BEAT THE WORLD'S HIGH SCORE!!!";
					var _msgW = string_width(_txt);
					var _msgH = string_height(_txt);
					draw_text_transformed(_w/2, 340, _txt, 0.4, 0.4, 0);
					
					var _color = $00ccff;
					part_type_color1(particle_type_highscore, _color);
		
					// Emit
					part_emitter_region(Game.particle_system, Game.particle_emitter, _w/2-_msgW/2, _w/2+_msgW/2, 340-_msgH/2, 340+_msgH/2, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_highscore, 40);	
				}
				else if (Game.total_score >= _number_5_score) {
					var _txt = "YOU REACHED THE WORLD's TOP 5!!!"
					var _msgW = string_width(_txt);
					var _msgH = string_height(_txt);
					draw_text_transformed(_w/2+2, 340, _txt, 0.4, 0.4, 0);
					
					var _color = c_silver;					
					part_type_color1(particle_type_highscore, _color);
		
					// Emit
					part_emitter_region(Game.particle_system, Game.particle_emitter, _w/2-_msgW/2, _w/2+_msgW/2, 340-_msgH/2, 340+_msgH/2, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_highscore, 40);	
				}
				*/
				
			}
			else if (http_return_status_query == noone) {
				//draw_text_transformed(_w/2, 400, "Loading scoreboard...", 0.7, 0.7, 0);
				type_formatted(_w/2, 400, "[fa_center][fa_middle][scale,0.7]Loading scoreboard...");
			}
			/*
			else  {
				draw_set_halign(fa_center);
				draw_text_transformed(_w/2, 400, "No/bad connection", 0.7, 0.7, 0);				
				draw_text_transformed(_w/2, 440, "Cannot show scoreboard", 0.7, 0.7, 0);				
			}*/
		}
	}
	
	fnc_Link(_w/2, _h-120, _link_color+"[fa_center][fa_middle][fnt_Details]ENTER to restart", "[fa_center][fa_middle][fnt_Details][$0BFFE6]ENTER to restart", fnc_TryAgain, 0);
	//fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle][fnt_Details]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][fnt_Details][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
	fnc_Link(_w/2, _h-60, _link_color+"[fnt_Details][fa_middle][fa_center][scale,0.8][$dddddd]Return to Main Menu", "[fnt_Details][fa_middle][fa_center][scale,0.8][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawHUD() {	
	
	
	fnc_BackupDrawParams();
	// Draw the HUD
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _title_color = "[c_white]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _health_color = "c_white";
	if (round(obj_Player.hp) <= 10) {
		var _health_color = "c_red";
	}
	else if (round(obj_Player.hp) <= 20) {
		var _health_color = "c_gold";
	}
	
	type_formatted(40, 40, "[fnt_Details][fa_left][fa_middle][blend,"+_health_color+"][spr_UI,8][scale,0.5]["+_health_color+"] "+string(round(obj_Player.hp))+"%");
	type_formatted(40, 80, "[fnt_Details][c_white][fa_left][fa_middle][spr_UI,10][scale,0.5] "+fnc_FormatString(string(Game.total_gold)));
	type_formatted(40, 120, "[fnt_Details][c_white][fa_left][fa_middle][spr_UI,9][scale,0.5] "+string(Game.current_depth)+" [c_gray]< "+string(Game.max_depth));
	
	var _battery_sprite = (obj_Player.helmet_light_battery > 50 ? "[spr_UI,0]" : (obj_Player.helmet_light_battery > 25 ? "[spr_UI,1]" : (obj_Player.helmet_light_battery > 10 ? "[spr_UI,2]" : "[spr_UI,3]")));
	var _light_level = obj_Player.helmet_light_level+1;
	//type_formatted(_w-40, 40, "[fnt_Details][c_white][fa_right][fa_middle][scale,0.2]LEVEL "+string(_light_level)+" [scale,1]"+_battery_sprite+"[scale,0.5] "+string(round(obj_Player.helmet_light_battery))+"%");
	type_formatted(40, 160, "[fnt_Details][c_white][fa_left][fa_middle][scale,0.2]LEVEL "+string(_light_level)+" [scale,1]"+_battery_sprite+"[scale,0.5] "+string(round(obj_Player.helmet_light_battery))+"%");
	var _pickaxe_level = obj_Player.pickaxe_level+1;
	//type_formatted(_w-40, 80, "[fnt_Details][c_white][fa_right][fa_middle][scale,0.2]LEVEL "+string(_pickaxe_level)+" [scale,1][spr_UI,7] [scale,0.5]"+string(round(100*obj_Player.pickaxe_health/obj_Player.pickaxe_max_health))+"%");
	type_formatted(40, 200, "[fnt_Details][c_white][fa_left][fa_middle][scale,0.2]LEVEL "+string(_pickaxe_level)+" [scale,1][spr_UI,7] [scale,0.5]"+string(round(100*obj_Player.pickaxe_health/obj_Player.pickaxe_max_health))+"%");
	var _bag_level = obj_Player.bag_level + 1;
	var _bag_sprite = (_bag_level == 1 ? "[spr_UI,4]" : (_bag_level == 2 ? "[spr_UI,5]" : "[spr_UI,6]"));
	//type_formatted(_w-40, 120, "[fnt_Details][c_white][fa_right][fa_middle]"+_bag_sprite+" [scale,0.5]"+string(obj_Player.bag_current_weight)+"[c_gray]/"+string(obj_Player.bag_max_weight));
	
	var _txt = "[fnt_Details][c_white][fa_center][fa_middle][scale,0.2]LEVEL "+string(_bag_level)+" [scale,1]"+_bag_sprite+" [scale,0.5]"+string(obj_Player.bag_current_weight)+"[c_gray]/"+string(obj_Player.bag_max_weight);
	var _result = type_formatted(_w-40, _h-40, _txt, false);
	var _txt_x = _w-40-_result.bbox_width/2-20;
	var _txt_y = _h-40-_result.bbox_height/2;
	type_formatted(_txt_x, _txt_y, _txt, true);
	
	// Jewel detail
	if (device_mouse_x_to_gui(0) > _txt_x - _result.bbox_width/2 && device_mouse_x_to_gui(0) < _txt_x + _result.bbox_width/2 && device_mouse_y_to_gui(0) > _txt_y - _result.bbox_height/2 && device_mouse_y_to_gui(0) < _txt_y + _result.bbox_height/2) {
		var _n = array_length(Game.jewels);
		var _jewel_txt;
		var _k = 0;
		for (var _i=0; _i<_n; _i++) {
			if (Game.jewels[_i].currently_have > 0) {
				_k++;
				_jewel_txt = "[fa_right][fa_middle][c_white][fnt_Details][spr_Jewels,"+string(Game.jewels[_i].index)+"][scale,0.4] "+Game.jewels[_i].name+" x"+string(Game.jewels[_i].currently_have);
				type_formatted(_w-60, _h-40-_result.bbox_height-40*(_k+1), _jewel_txt);
			}
		}
		//type_formatted(_w-60, _h-40-_result.bbox_height-40, "[fa_right][fa_middle][c_white][fnt_Details][scale,0.5]Total value: $"+string(fnc_FormatString(fnc_CurrentValue())));
		
		type_formatted(_w-60, _h-40-_result.bbox_height-40, "[fa_right][fa_middle][c_white][fnt_Details][scale,0.5]Total value: $"+fnc_FormatString(string(fnc_CurrentValue())));
	}
	
		
	var _link_text = "[fa_right][fa_middle][fnt_Details][scale, 0.4]"+_link_color+"Call for rescue";
	var _link_hover_text = "[fa_right][fa_middle][fnt_Details][scale, 0.4]"+_link_hover_color+"> Call for rescue";	
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_red]";
	
	if (Game.show_rescue_confirmation_dialog) {
		draw_set_alpha(0.5);
		draw_roundrect_color_ext(_w/2-400, _h/2-100, _w/2+500, _h/2+100, 15, 15, c_black, c_black, false);
		draw_set_alpha(1);
		type_formatted(_w/2, _h/2, "[fnt_Details][scale,0.6][fa_middle][fa_center][c_white]Are you sure? You will lose $"+fnc_FormatString(string(fnc_CurrentValue()))+" worth of gems!");
		fnc_Link(_w/2-100, _h/2+40, "[fnt_Details][scale,0.6][fa_middle][fa_center][c_gray]Yes", "[fnt_Details][scale,0.6][fa_middle][fa_center][c_white]>Yes", fnc_Rescue, true);
		fnc_Link(_w/2+100, _h/2+40, "[fnt_Details][scale,0.6][fa_middle][fa_center][c_gray]No", "[fnt_Details][scale,0.6][fa_middle][fa_center][c_white]>No", fnc_Rescue, false);
	}
	else if (Game.current_depth > 0) {
		fnc_Link(_w-60, _h-40, _link_text, _link_hover_text, fnc_Rescue, 0);
	}

	if (Game.show_tutorial_message) {
		draw_set_alpha(0.5);
		draw_roundrect_color_ext(100, _h-200, _w-100, _h-100, 15, 15, c_black, c_black, false);
		draw_set_alpha(1);
		type_formatted(_w/2, _h-150, tutorial[tutorial_index]);
	}
	
	if (Game.rescue_transition_type == 1) {		
		draw_set_alpha(1-Game.alarm[6]/RESCUE_TRANSITION_TIME);
		draw_rectangle_color(0, 0, window_get_width(), window_get_height(), c_black, c_black, c_black, c_black, false);
		draw_set_alpha(1);
	}
	else if (Game.rescue_transition_type == 2) {
		draw_set_alpha(Game.alarm[6]/RESCUE_TRANSITION_TIME);
		draw_rectangle_color(0, 0, window_get_width(), window_get_height(), c_black, c_black, c_black, c_black, false);
		draw_set_alpha(1);
	}
	
	fnc_RestoreDrawParams();
}


function fnc_Rescue() {
	if (!Game.show_rescue_confirmation_dialog) {
		Game.show_rescue_confirmation_dialog = true;
	}
	else if (argument[0]) {
		Game.rescue_transition_type = 1;
		Game.alarm[6] = RESCUE_TRANSITION_TIME;
		obj_Player.controllable = false;
		Game.show_rescue_confirmation_dialog = false;
	}
	else {		
		Game.show_rescue_confirmation_dialog = false;
	}
}

// SHOP
function fnc_DrawShop() {
	
	fnc_BackupDrawParams();
	// Draw the HUD
	var _w = window_get_width();
	var _h = window_get_height();
	
	
	var _title_color = "[c_white]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _rect_x1 = 200;
	var _rect_x2 = 700;
	var _rect_y1 = 40;
	var _rect_y2 = window_get_height()-40;
	
	var _rect_detail_x1 = _rect_x2 + 40;
	var _rect_detail_x2 = _w-40;
	var _rect_detail_y1 = 40;
	var _rect_detail_y2 = _h/2;
	
	draw_set_alpha(0.9);	
	draw_rectangle_color(_rect_x1, _rect_y1, _rect_x2, _rect_y2, $444444, $555555, $444444, $555555, false);
	draw_set_alpha(1);
	draw_rectangle_color(_rect_x1, _rect_y1, _rect_x2, _rect_y2, $333333, $333333, $333333, $333333, true);
	
	draw_set_alpha(0.9);	
	draw_rectangle_color(_rect_detail_x1, _rect_detail_y1, _rect_detail_x2, _rect_detail_y2, $444444, $555555, $444444, $555555, false);
	draw_set_alpha(1);
	draw_rectangle_color(_rect_detail_x1, _rect_detail_y1, _rect_detail_x2, _rect_detail_y2, $333333, $333333, $333333, $333333, true);
	
	
	type_formatted( _rect_x1+(_rect_x2-_rect_x1)/2, _rect_y1+40, "[fnt_Details][fa_center][fa_middle][c_white][scale,0.7]Mine Shop");
	
	// Draw shop items
	type_formatted(_rect_x1 + 40 + 10, _rect_y1 + 80, "[fnt_Details][fa_top][fa_left][scale,0.4][c_white]Available stock:");
	var _n = array_length(Game.shop);
	for (var _i=0; _i<_n; _i++) {
		var _item = Game.shop[_i];		
		if (_item.unlocked && _item.currently_available() && _item.price <= Game.total_gold && _item.shop_availability != 0) {
			var _color = "[c_white]";
			var _color_hover = "[c_yellow]";
			var _function = fnc_BuyItem;
		}		
		else if (_item.unlocked && _item.currently_available() && _item.price > Game.total_gold && _item.shop_availability != 0) {
			var _color = "[c_red]";
			var _color_hover = "[c_red]";
			var _function = fnc_NotEnough;
		}
		else if (!_item.unlocked) {
			var _color = "[c_black]";
			var _color_hover = "[c_black]";
			var _function = fnc_NotUnlocked;
		}
		else if (!_item.currently_available() || _item.shop_availability == 0) {
			var _color = "[$aaaaaa]";
			var _color_hover = "[$aaaaaa]";
			var _function = fnc_NotAvailable;
		}
		
		//draw_roundrect_color_ext(_rect_x1 + 40+_i*20, _rect_y1 + 80 + _i*40, _rect_x2-40, _rect_y1+80+_i*40 + 60, 15, 15, $eeeeee, $eeeeee, false);
		var _link_format = "[fnt_Details][fa_top][fa_left][scale,0.3]";
		var _description_format = "[fnt_Details][fa_top][fa_left][scale,0.5][c_white]";
		fnc_Link(_rect_x1 + 40 + 10, _rect_y1 + 80 + (_i+1)*27 + 15, _link_format+_color+_item.name, _link_format+_color_hover+"> "+_item.name, _function, _i, 
				_rect_detail_x1 + 40 + 10, _rect_y1+40, _description_format+_item.name, "[fnt_Details][fa_top][fa_left][scale,0.3][$dddddd]"+string(_item.description));
		type_formatted(_rect_x2-50, _rect_y1 + 80 + (_i+1)*27 + 15, "[fnt_Details][fa_top][fa_right][scale,0.3]"+_color+"$ "+fnc_FormatString(string(_item.price)));
		
	}
	
	// Shop message
	if (shop_message != "") {
		type_formatted(_rect_detail_x1 + 40 + 10, _rect_detail_y2 - 80, shop_message);
	}
	
	
	// Sell
	var _value = fnc_CurrentValue();
	if (_value > 0) {
		fnc_Link(_rect_x1 + 40 + 10, _rect_y2 -40, "[fnt_Details][fa_bottom][fa_left][scale,0.4][$dddddd]Sell stash for $"+fnc_FormatString(string(_value)), "[fnt_Details][fa_bottom][fa_left][scale,0.4][c_white]> Sell stash for  $"+fnc_FormatString(string(_value)), fnc_Sell, 0);
	}
	else {
		type_formatted(_rect_x1 + 40 + 10, _rect_y2 -40, "[fnt_Details][fa_bottom][fa_left][scale,0.4][$dddddd]You are not carrying stuff! Go mine!");
	}
	
	
	
	
	// Exit shop
	type_formatted(_rect_detail_x1 + 40 + 10, _rect_detail_y2 -20, "[fnt_Details][fa_bottom][fa_left][scale,0.4][c_white]Right-click to exit shop");
	
	
	//if (device_mouse_check_button_pressed(0, mb_right) && !(device_mouse_x_to_gui(0) > _rect_x1 && device_mouse_x_to_gui(0) < _rect_x2 && device_mouse_y_to_gui(0) > _rect_y1 && device_mouse_y_to_gui(0) < _rect_y2)) {
	if (device_mouse_check_button_pressed(0, mb_right)) {
		draw_shop = false;	
		obj_Player.controllable = true;
	}
	
	fnc_RestoreDrawParams();
}

function fnc_BuyItem(_item) {
	Game.shop_message = "[fnt_Details][fa_middle][fa_left][c_white][scale, 0.4]Thanks for your purchase, come back!"
	Game.alarm[2] = SHOP_MESSAGE_TIME;	

	Game.total_gold = Game.total_gold - Game.shop[_item].price;
	switch (_item) {
		case 0: // replace battery
			obj_Player.helmet_light_battery = 100;
			obj_Player.current_light_size = Game.headlight_level_strength[obj_Player.helmet_light_level];
			obj_Player.helmet_light.light.xscale = obj_Player.current_light_size;
			obj_Player.helmet_light.light.yscale = obj_Player.current_light_size;
			obj_Player.helmet_light.light.blend = Game.headlight_level_color[obj_Player.helmet_light_level];
			break;
		case 1: // repair pickaxe
			obj_Player.pickaxe_health = obj_Player.pickaxe_max_health;
			break;
		case 2: // bandaid
			obj_Player.hp = min(obj_Player.max_hp, obj_Player.hp + 10);
			break;
		case 3: // first aid
			obj_Player.hp = obj_Player.max_hp;
			break;
		case 4: // pickaxe level 2
			obj_Player.pickaxe_level++;
			obj_Player.pickaxe_max_health = Game.pickaxe_level_durability[1];
			obj_Player.pickaxe_health = obj_Player.pickaxe_max_health;
			obj_Player.pickaxe_damage = Game.pickaxe_level_strength[1];
			break;
		case 5: // pickaxe level 3
			obj_Player.pickaxe_level++;
			obj_Player.pickaxe_max_health = Game.pickaxe_level_durability[2];
			obj_Player.pickaxe_health = obj_Player.pickaxe_max_health;
			obj_Player.pickaxe_damage = Game.pickaxe_level_strength[2];
			break;
		case 6: // pickaxe level 4
			obj_Player.pickaxe_level++;
			obj_Player.pickaxe_max_health = Game.pickaxe_level_durability[3];
			obj_Player.pickaxe_health = obj_Player.pickaxe_max_health;
			obj_Player.pickaxe_damage = Game.pickaxe_level_strength[3];;
			break;
		case 7: // medium bag
			obj_Player.bag_level++;
			obj_Player.bag_max_weight = Game.bag_level_capacity[1];
			break;
		case 8: // big bag
			obj_Player.bag_level++;
			obj_Player.bag_max_weight = Game.bag_level_capacity[2];
			break;
		case 9: // light level 2
			obj_Player.helmet_light_level++;
			obj_Player.current_light_size = Game.headlight_level_strength[obj_Player.helmet_light_level];
			obj_Player.helmet_light.light.xscale = obj_Player.current_light_size;
			obj_Player.helmet_light.light.yscale = obj_Player.current_light_size;
			obj_Player.helmet_light.light.blend = Game.headlight_level_color[obj_Player.helmet_light_level];
			break;
		case 10: // light level 3
			obj_Player.helmet_light_level++;
			obj_Player.current_light_size = Game.headlight_level_strength[obj_Player.helmet_light_level];
			obj_Player.helmet_light.light.xscale = obj_Player.current_light_size;
			obj_Player.helmet_light.light.yscale = obj_Player.current_light_size;
			obj_Player.helmet_light.light.blend = Game.headlight_level_color[obj_Player.helmet_light_level];
			break;
		default:
			break;
	}
}

function fnc_NotEnough(_item) {	
	Game.shop_message = "[fnt_Details][fa_middle][fa_left][c_red][scale, 0.4]Not enough money!"
	Game.alarm[2] = SHOP_MESSAGE_TIME;
	//show_debug_message("Not enough money "+string(_item));
}

function fnc_NotUnlocked(_item) {
	Game.shop_message = "[fnt_Details][fa_middle][fa_left][c_black][scale, 0.4]Item not unlocked!"
	Game.alarm[2] = SHOP_MESSAGE_TIME90;	
	//show_debug_message("Not unlocked "+string(_item));
}

function fnc_NotAvailable(_item) {
	Game.shop_message = "[fnt_Details][fa_middle][fa_left][$aaaaaa][scale, 0.4]Currently out of stock!"
	Game.alarm[2] = SHOP_MESSAGE_TIME;	
	//show_debug_message("Not available "+string(_item));
}

function fnc_CurrentValue() {
	var _n = array_length(Game.jewels);
	var _value = 0;
	for (var _i=0; _i<_n; _i++) {
		var _item = Game.jewels[_i];
		_value = _value + _item.currently_have * _item.value;
	}
	return _value;
}

function fnc_HistoricGems() {
	var _n = array_length(Game.jewels);
	var _value = 0;
	for (var _i=0; _i<_n; _i++) {
		var _item = Game.jewels[_i];
		_value = _value + _item.total_had;
	}
	return _value;
}


function fnc_HistoricValue() {
	var _n = array_length(Game.jewels);
	var _value = 0;
	for (var _i=0; _i<_n; _i++) {
		var _item = Game.jewels[_i];
		_value = _value + _item.total_had * _item.value;
	}
	return _value;
}

function fnc_Sell() {
	Game.total_gold = Game.total_gold + fnc_CurrentValue();
	fnc_Dispose();	
	Game.shop_message = "[fnt_Details][fa_middle][fa_left][c_white][scale, 0.4]Thank you for your business!"
	Game.alarm[2] = SHOP_MESSAGE_TIME;	
}

function fnc_Dispose() {
	var _n = array_length(Game.jewels);
	for (var _i=0; _i<_n; _i++) {
		var _item = Game.jewels[_i];
		_item.currently_have = 0;
	}
	obj_Player.bag_current_weight = 0;
}

#endregion













#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.7);
		//draw_rectangle(0, 0, adjusted_window_width, adjusted_window_height, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(10, 10,	"DEBUG MODE");
		if (instance_exists(obj_Player)) {
			draw_text(10, 25, "Player: "+string(obj_Player.x)+","+string(obj_Player.y)+" / State="+obj_Player.state_name);
		}
		else {
			draw_text(10, 25, "Player not in room");	
		}
		var _spacing = 30;
		
		var _debug_data = [];
		
		_debug_data[0] = "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")";
		_debug_data[1] = "Requested scaling type: "+string(SELECTED_SCALING)+" ("+(SELECTED_SCALING==SCALING_TYPE.WINDOW_SAME_AS_RESOLUTION ? "Window determined by resolution" : (SELECTED_SCALING == SCALING_TYPE.WINDOW_INDEPENDENT_OF_RESOLUTION ? "Window independent of resolution" : "Resolution scaled to window"));
		_debug_data[2] = "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H);
		_debug_data[3] = "Actual base resolution: "+string(adjusted_camera_width)+"x"+string(adjusted_camera_height);
		_debug_data[4] = "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H);
		_debug_data[5] = "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height);
		_debug_data[6] = "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")";
		_debug_data[7] = "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")";
		_debug_data[8] = "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")";
		_debug_data[9] = "GUI Layer: "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")";
		_debug_data[10] = "Mouse: "+string(mouse_x)+","+string(mouse_y);
		_debug_data[11] = "Device mouse 0: "+string(device_mouse_x(0))+","+string(device_mouse_y(0));
		_debug_data[12] = "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0));		
		_debug_data[13] = "Paused: "+string(paused);
		_debug_data[14] = "FPS: "+string(fps_real) + "/" + string(fps);
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(10, 40+_i*15, _debug_data[_i]);
			//show_debug_message(_debug_data[_i]);
		}
	}
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _mousex = device_mouse_x_to_gui(0);
	var _mousey = device_mouse_y_to_gui(0);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = _mousex >= _bbox_coords[0] && _mousey >= _bbox_coords[1] && _mousex <= _bbox_coords[2] && _mousey <= _bbox_coords[3];
	//var _mouseover = _mousex >= _draw_data_normal.bbox_x1 && _mousey >= _draw_data_normal.bbox_y1 && _mousex <= _draw_data_normal.bbox_x2 && _mousey <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_white);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			script_execute(_callback, _param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (argument_count > 6) {
			var _x2 = argument[6];
			var _y2 = argument[7];
			var _text2 = argument[8];
			var _text3 = argument[9];
			type_formatted(_x2, _y2, _text2);
			type_formatted(_x2, _y2+_draw_data_normal.bbox_height+20, _text3);			
		}
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	script_execute(asset_get_index("fnc_Menu_"+string(_param)));
}

function fnc_ExecuteOptions(_param) {
	script_execute(asset_get_index("fnc_Options_"+string(_param)));
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion