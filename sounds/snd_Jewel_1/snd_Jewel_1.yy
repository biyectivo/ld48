{
  "compression": 0,
  "volume": 0.3,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Jewel_1.wav",
  "duration": 1.120102,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Jewel_1",
  "tags": [],
  "resourceType": "GMSound",
}